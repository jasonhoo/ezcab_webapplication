﻿'Creator       : Lee Ee Theng
'Create Date   : 24/10/2011
'Description   : 1. Create Product Listing Page

'History of update:
'No.        Auditor            Audit Date Time            Description                              
'1.  

Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports System.Data
Imports Telerik.Web.UI
Imports System.IO

Partial Class Admin_product_listing
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            LoadContent()
        End If
    End Sub

    Private Sub LoadContent()
        Dim objDB As Database
        Dim objDS As DataSet
        Dim strSql As String
        Dim objListItem As New ListItem
       
        objListItem.Text = "--Show All--"
        objListItem.Value = ""

        Try
            '// Load Category
            strSql = "SELECT RECID, Category FROM tblProduct_Category WHERE Status= 1 ORDER BY Category "
            '//Load Merchant
            strSql += "SELECT RECID, MerchName FROM tblMerchant WHERE Status = 1 ORDER BY MerchName "

            objDB = DatabaseFactory.CreateDatabase()
            objDS = objDB.ExecuteDataSet(CommandType.Text, strSql)

            ddlCategory.DataSource = objDS.Tables(0)
            ddlCategory.DataBind()
            ddlCategory.Items.Insert(0, objListItem)

            ddlMerchant.DataSource = objDS.Tables(1)
            ddlMerchant.DataBind()
            ddlMerchant.Items.Insert(0, objListItem)

        Catch ex As Exception
            lblMsg.Text = ex.Message + " - fail to load content."
        Finally
            objDB = Nothing
        End Try
    End Sub

    Protected Sub RadGrid1_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles RadGrid1.ItemCommand
        Dim strScript As String = ""
        Dim objDB As Database
        Dim strSql As String = ""
        Dim strPath As String = Request.PhysicalApplicationPath
        Dim strDirPath As String = strPath + "admin\upload\Product\"
        Select Case e.CommandName
            Case "DEL"
                Try
                    strSql += "DELETE FROM tblProduct_Master WHERE RECID ='" & e.CommandArgument & "' "
                    strSql += "DELETE FROM tblProduct_Location WHERE ProdRECID='" & e.CommandArgument & "' "
                    objDB = DatabaseFactory.CreateDatabase()
                    objDB.ExecuteNonQuery(CommandType.Text, strSql)

                    Dim strDir As String = strDirPath + e.CommandArgument

                    If Directory.Exists(strDir) = True Then
                        CommonLib.DeleteAllSubFolders(strDir)
                    End If

                    RadGrid1.Rebind()

                Catch ex As Exception
                    lblMsg.Text = ex.Message + " - fail to delete record."
                Finally
                    objDB = Nothing
                End Try
        End Select
    End Sub

    Protected Sub RadGrid1_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles RadGrid1.ItemDataBound
        Select Case e.Item.ItemType
            Case Telerik.Web.UI.GridItemType.Item, Telerik.Web.UI.GridItemType.AlternatingItem
                Dim objLbl As Label = e.Item.FindControl("lblAutoNo")
                objLbl.Text = (RadGrid1.MasterTableView.CurrentPageIndex * RadGrid1.MasterTableView.PageSize) + (e.Item.ItemIndex + 1)

                Dim targetView As Label = e.Item.FindControl("lblProdName")

                If Not Object.Equals(targetView, Nothing) Then
                    If Not Object.Equals(Me.RadToolTipManager2, Nothing) Then
                        Dim objRECID As String = e.Item.Cells(4).Text
                        RadToolTipManager2.TargetControls.Add(targetView.ClientID, objRECID, True)
                    End If
                End If

        End Select
    End Sub

    Protected Sub RadGrid1_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles RadGrid1.NeedDataSource
        Dim objDB As Database
        Dim objDS As DataSet
        Dim strSql As String = ""

        Try

            strSql += "SELECT RECID, ProdName,ProdCode, Credit , ProdShortName, StartDate, EndDate, MerchName, Category, Price_Discount , Price_RRP, CashVoucher, DiscountVoucher, TotalBought "
            strSql += ", CASE WHEN IsGroup = 1 THEN (SELECT GroupCode FROM tblGroup WHERE RECID = GroupRECID) WHEN IsGroup = 0 THEN 'Ungroup' END AS GroupCode "
            strSql += ", CASE PromotionType  WHEN 'PP' THEN 'Promotion Price' WHEN 'CV' THEN 'Cash Voucher' WHEN 'DV' THEN 'Discount Voucher' END AS PromotionType "
            strSql += ", CASE WHEN LEN(ProdName) > 60 THEN LEFT(ProdName, 60) + '...' ELSE ProdName END AS ProdNameLink "
            strSql += ", CASE WHEN NOT Item_Img1 IS NULL THEN 'upload/product/' + CONVERT(VARCHAR(50),RECID) + '/' + Item_Img1 END AS ImgLink "
            strSql += ", CASE WHEN TotalQty IS NULL THEN 'Unlimited' ELSE CONVERT(VARCHAR(50),TotalQty) End As Qty "
            strSql += ", CASE WHEN Status=1 THEN 'Publish' WHEN Status =0  THEN 'Draft' END AS Status "
            strSql += " FROM vw_Product  WHERE NOT RECID IS NULL "

            If Len(txtName.Text) > 0 Then
                strSql += " AND ProdName LIKE '%" & Replace(txtName.Text, "'", "''") & "' "
            End If

            If ddlPeriod.SelectedIndex = 0 Then
                If Len(rdpProStartFrom.DbSelectedDate) > 0 Then
                    strSql += " AND DateDiff(d, '" & Format(rdpProStartFrom.DbSelectedDate, "dd/MMM/yyyy") & "',StartDate) >= 0 "
                End If

                If Len(rdpProStartTo.DbSelectedDate) < 0 Then
                    strSql += " AND DateDiff(d, '" & Format(rdpProStartTo.DbSelectedDate, "dd/MMM/yyyy") & "',StartDate)<= 0 "
                End If

                If Len(rdpProEndFrom.DbSelectedDate) > 0 Then
                    strSql += " AND DateDiff(d, '" & Format(rdpProEndFrom.DbSelectedDate, "dd/MMM/yyyy") & "',EndDate) >= 0 "
                End If

                If Len(rdpProEndTo.DbSelectedDate) > 0 Then
                    strSql += " AND DateDiff(d, '" & Format(rdpProEndTo.DbSelectedDate, "dd/MMM/yyyy") & "',EndDate) <= 0 "
                End If
            End If

            If ddlStatus.SelectedIndex > 0 Then
                strSql += " AND Status = " & ddlStatus.SelectedValue & " "
            End If

            If ddlPeriod.SelectedIndex > 0 Then
                If ddlPeriod.SelectedValue = "A" Then
                    strSql += " AND DateDiff(hour, StartDate, getDate()) >= 0 AND DateDiff(d, EndDate, GetDate()) <= 0 "
                End If
                If ddlPeriod.SelectedValue = "P" Then
                    strSql += " AND DateDiff(d, EndDate, GetDate()) >= 0 And DateDiff(d, StartDate, GetDate()) >=0 "
                End If
                If ddlPeriod.SelectedValue = "C" Then
                    strSql += "AND DateDiff(hour, StartDate, getDate()) <= 0 AND DateDiff(d, EndDate, GetDate()) <= 0  "
                End If
            End If

            If ddlMerchant.SelectedIndex > 0 Then
                strSql += " AND MerchRECID ='" & Replace(ddlMerchant.SelectedValue, "'", "''") & "' "
            End If

            If ddlCategory.SelectedIndex > 0 Then
                strSql += " AND ProdCatRECID ='" & Replace(ddlCategory.SelectedValue, "'", "''") & "' "
            End If

            If ddlPromotionType.SelectedIndex > 0 Then
                strSql += " AND PromotionType ='" & Replace(ddlPromotionType.SelectedValue, "'", "''") & "' "
            End If


            strSql += " ORDER BY SeqNo DESC "

            objDB = DatabaseFactory.CreateDatabase()
            objDS = objDB.ExecuteDataSet(CommandType.Text, strSql)

            RadGrid1.DataSource = objDS.Tables(0)
        Catch ex As Exception
            lblMsg.Text = ex.Message + "-" & strSql & " - fail to load grid."
        Finally
            objDB = Nothing
        End Try
    End Sub

    Protected Sub lnkSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSearch.Click
        RadGrid1.Rebind()
    End Sub

    Protected Sub lnkDraft_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDraft.Click
        Dim objRow As GridItem
        Dim strSQL As String = ""
        Dim strRECID As String = ""
        Dim objDB As Database
        Dim ArrUpdate As New ArrayList
        Dim strUserID As String = Request.Cookies(CommonLib.COOKIE_USERLOGIN)(CommonLib.COOKIE_LOGINID).ToString
        Dim strScript As String = "<script language=""javascript"" type=""text/javascript""> "

        If RadGrid1.SelectedItems.Count = 0 Then
            strScript += " alert('Please select at least one deal.');"
            strScript += " </script>"
            ClientScript.RegisterStartupScript(Page.GetType(), "Alert_Redirect_Update", strScript)
            Exit Sub
        End If

        objDB = DatabaseFactory.CreateDatabase()

        '// Update Deal Status
        Try
            For Each objRow In RadGrid1.SelectedItems
                strRECID = objRow.Cells(4).Text
                strSQL += "UPDATE tblProduct_Master SET Status = 0  WHERE RECID ='" & strRECID & "' "

            Next
            objDB.ExecuteNonQuery(CommandType.Text, strSQL)

            RadGrid1.Rebind()

            strScript += " alert('Deals save as draft.');"
            strScript += " </script>"
            ClientScript.RegisterStartupScript(Page.GetType(), "Alert_Redirect_Update", strScript)

        Catch ex As Exception
            lblMsg.Text = "* " + ex.Message + " - fail to get deal Listing"
        Finally
            objDB = Nothing
        End Try
    End Sub

    Protected Sub lnkPublish_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPublish.Click
        Dim objRow As GridItem
        Dim strSQL As String = ""
        Dim strRECID As String = ""
        Dim objDB As Database
        Dim ArrUpdate As New ArrayList
        Dim strUserID As String = Request.Cookies(CommonLib.COOKIE_USERLOGIN)(CommonLib.COOKIE_LOGINID).ToString
        Dim strScript As String = "<script language=""javascript"" type=""text/javascript""> "

        If RadGrid1.SelectedItems.Count = 0 Then
            strScript += " alert('Please select at least one deal to publish.');"
            strScript += " </script>"
            ClientScript.RegisterStartupScript(Page.GetType(), "Alert_Redirect_Update", strScript)
            Exit Sub
        End If

        objDB = DatabaseFactory.CreateDatabase()

        '// Update Deal Status
        Try
            For Each objRow In RadGrid1.SelectedItems
                strRECID = objRow.Cells(4).Text
                strSQL += "UPDATE tblProduct_Master SET Status = 1 WHERE RECID ='" & strRECID & "' "

            Next
            objDB.ExecuteNonQuery(CommandType.Text, strSQL)

            RadGrid1.Rebind()

            strScript += " alert('Deals Published.');"
            strScript += " </script>"
            ClientScript.RegisterStartupScript(Page.GetType(), "Alert_Redirect_Update", strScript)

        Catch ex As Exception
            lblMsg.Text = "* " + ex.Message + " - fail to get deal Listing"
        Finally
            objDB = Nothing
        End Try
    End Sub

    Protected Sub OnAjaxUPdate2(ByVal sender As Object, ByVal e As ToolTipUpdateEventArgs) Handles RadToolTipManager2.AjaxUpdate
        UpdateToolTip2(e.Value, e.UpdatePanel)
    End Sub

    Private Sub UpdateToolTip2(ByVal elementID As String, ByVal panel As UpdatePanel)
        Dim strToolTipLink As String
        strToolTipLink = "View_ProductDetails.ascx"
        Dim ctrl As Control = Page.LoadControl(strToolTipLink)
        panel.ContentTemplateContainer.Controls.Add(ctrl)
        Dim details As Admin_View_ProductDetails = DirectCast(ctrl, Admin_View_ProductDetails)
        details.RECID = elementID
    End Sub
End Class
