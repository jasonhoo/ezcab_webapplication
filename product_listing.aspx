﻿<%@ Page Language="VB" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="false" CodeFile="product_listing.aspx.vb" Inherits="Admin_product_listing" %>
<%@ Register src="View_ProductDetails.ascx" tagname="View_ProductDetails" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftPanel" Runat="Server">
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server" >
    <AjaxSettings >
<telerik:AjaxSetting AjaxControlID ="lnkSearch">
<UpdatedControls >
<telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="RadAjaxLoadingPanel1" />
<telerik:AjaxUpdatedControl ControlID="RadToolTipManager2" />
</UpdatedControls>
</telerik:AjaxSetting>
<telerik:AjaxSetting AjaxControlID ="RadGrid1">
<UpdatedControls >
<telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="RadAjaxLoadingPanel1" />
<telerik:AjaxUpdatedControl ControlID="RadToolTipManager2" />
</UpdatedControls>
</telerik:AjaxSetting>
</AjaxSettings>
</telerik:RadAjaxManagerProxy>
<table border="0" cellpadding="0" cellspacing="0" width="250" style="border-collapse: collapse">
<tr>
    <td>
        <telerik:RadPanelBar ID="RadPanelBar1" width="100%" Runat="server" CausesValidation="False" Skin="Web20">
            <Items>
                <telerik:RadPanelItem ImageUrl="pic/blockarrow.gif" ImagePosition="Left" Text="Shortcut Menu" Expanded="true">
                    <Items>
                        <telerik:RadPanelItem ImageUrl="pic/ico-add-pro.gif" ImagePosition="Left" NavigateUrl="product_Details.aspx" Text="Add Deal"></telerik:RadPanelItem>
                        <telerik:RadPanelItem ImageUrl="pic/ico-Seq-24.gif" ImagePosition="Left" NavigateUrl="product_Sorting.aspx" Text="Sort Deal"></telerik:RadPanelItem>
                      </Items>
                </telerik:RadPanelItem>
            </Items>
        </telerik:RadPanelBar>
    </td>
</tr>
</table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" Runat="Server">
<telerik:RadCodeBlock runat="server" ID="RadCodeBlock">
<style type="text/css">      
   div.RadGrid .rgPager .rgAdvPart     
   {     
    display:none;        
   }      
</style> 
</telerik:RadCodeBlock>

<table border="0" cellpadding="0" width="100%" id="table154" style="border-collapse: collapse" bordercolor="#DFDFDF">
<tr>
    <td>
    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="table155">
    <tr>
        <td bgcolor="#fcb054" height="3" align="left"><asp:Label runat="server" ID="lblMsg" CssClass="con11" ForeColor="Red"></asp:Label></td></tr>
        <tr><td align="left" height="10"></td></tr>
        <tr>
            <td align="left" >
                <telerik:RadTabStrip runat="server" ID="RadTabStrip" Width="100%" CausesValidation="False" Skin="Vista" SelectedIndex="0">
                 <Tabs>
                      <telerik:RadTab Text="All Deals Listing" NavigateUrl="product_listing.aspx" ></telerik:RadTab>
                      <telerik:RadTab Text="Today's Active Deals" NavigateUrl="product_active_listing.aspx" ></telerik:RadTab>
                      <telerik:RadTab Text="Coming Soon Deals" NavigateUrl="product_coming_listing.aspx"  ></telerik:RadTab>
                      <telerik:RadTab Text="Past Deals" NavigateUrl="product_past_listing.aspx" ></telerik:RadTab>
                      <telerik:RadTab Text="Draft" NavigateUrl="product_draft_listing.aspx"  ></telerik:RadTab>
                      <telerik:RadTab Text="Sort Deals" NavigateUrl="product_sorting.aspx" ></telerik:RadTab>
                      <telerik:RadTab Text="Add Deals" NavigateUrl="product_details.aspx"  ></telerik:RadTab>
                 </Tabs>
                </telerik:RadTabStrip>
            </td>
        </tr>
        <tr>
            <td style="border: 1px solid #939393;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                  <tr><td height="26" valign="bottom" style="padding-left:10px; padding-right:10px">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="table156">
                    <tr>
                        <td width="50%" align="left">
                            <img border="0" src="pic/blueblockarrow.gif" width="8" height="8" align="absmiddle"><span class="pgtit"><font color="#FFFFFF"></font>&nbsp;All Deals</span>
                        </td>
                        <td align="right" class="con11" width="50%"></td>
                    </tr>
                    </table>
                </td>
                </tr>
                 <tr><td style="padding:10px">
<table border="1" cellpadding="5" width="100%" id="table170" style="border-collapse: collapse" bordercolor="#DFDFDF" cellspacing="0">
<tr><td align="left">
<table border="0" cellpadding="5" cellspacing="0" id="table171" class="con12" width="100%">
<tr>
    <td width="90" align="left">Deal Name</td>
    <td align="center" width="10">:</td>
    <td align="left" colspan="4"><asp:TextBox ID="txtName" runat="server" Width="700px" ></asp:TextBox></td>
</tr>
<tr>
    <td align="left" valign="top">Promotion Start</td>
    <td align="center" valign="top">:</td>
    <td align="left" valign="top">
        <telerik:RadDatePicker runat="server" ID="rdpProStartFrom"><DateInput DateFormat="dd/MM/yyyy"></DateInput></telerik:RadDatePicker>
        </td>
    <td align="left" valign="top">To</td>
    <td align="center" valign="top">:</td>
    <td align="left" valign="top">
        <telerik:RadDatePicker runat="server" ID="rdpProStartTo"><DateInput DateFormat="dd/MM/yyyy"></DateInput></telerik:RadDatePicker>
        </td>
</tr>
<tr>
    <td align="left" valign="top">Promotion End</td>
    <td align="center" valign="top">:</td>
    <td align="left" valign="top">
        <telerik:RadDatePicker runat="server" ID="rdpProEndFrom"><DateInput DateFormat="dd/MM/yyyy"></DateInput></telerik:RadDatePicker>
        </td>
    <td align="left" valign="top">To</td>
    <td align="center" valign="top">:</td>
    <td align="left" valign="top">
        <telerik:RadDatePicker runat="server" ID="rdpProEndTo"><DateInput DateFormat="dd/MM/yyyy"></DateInput></telerik:RadDatePicker>
        </td>
</tr>
<tr>
    <td align="left" valign="top">Merchant</td>
    <td align="center" valign="top">:</td>
    <td align="left" valign="top">
        <asp:DropDownList ID="ddlMerchant" runat="server" width="250px" DataTextField="MerchName" DataValueField="RECID"></asp:DropDownList>
        </td>
    <td align="left" valign="top">Category</td>
    <td align="center" valign="top">:</td>
    <td align="left" valign="top">
        <asp:DropDownList ID="ddlCategory" runat="server" width="250px"
            DataTextField="Category" DataValueField="RECID"></asp:DropDownList>
        </td>
</tr>
<tr>
    <td align="left" valign="top">Promotion Type</td>
    <td align="center" valign="top">:</td>
    <td align="left" class="style1">
        <asp:DropDownList ID="ddlPromotionType" runat="server" width="250px">
            <asp:ListItem Value="">--Show All--</asp:ListItem>
            <asp:ListItem Value="PP">Promotion Price</asp:ListItem>
            <asp:ListItem Value="CV">Cash Voucher</asp:ListItem>
            <asp:ListItem Value="DV">Discount Voucher</asp:ListItem>
        </asp:DropDownList>
    </td>
    <td align="left" valign="top">Period</td>
    <td align="center" valign="top">:</td>
    <td align="left">
        <asp:DropDownList ID="ddlPeriod" runat="server" DataTextField="MerchName" width="250px" DataValueField="RECID">
            <asp:ListItem>--Show All--</asp:ListItem>
            <asp:ListItem Value="A">Active Now</asp:ListItem>
            <asp:ListItem Value="P">Past </asp:ListItem>
            <asp:ListItem Value="C">Coming Soon</asp:ListItem>
        </asp:DropDownList>
    </td>
</tr>
<tr>
    <td align="left" valign="top">Status</td>
    <td align="center" valign="top">:</td>
    <td align="left" class="style1">
        <asp:DropDownList ID="ddlStatus" runat="server" DataTextField="MerchName" DataValueField="RECID" Width="250px">
            <asp:ListItem Value="">--Show All--</asp:ListItem>
            <asp:ListItem Value="1">Publish</asp:ListItem>
            <asp:ListItem Value="0">Draft</asp:ListItem>
        </asp:DropDownList>
    </td>
    <td align="left" valign="top">&nbsp;</td>
    <td align="center" valign="top">&nbsp;</td>
    <td align="left">
        &nbsp;</td>
</tr>
</table>
</td></tr>
<tr><td bgcolor="#F5F5F5" align="left">
<table border="0" cellpadding="0" cellspacing="0" id="table172" height="26">
<tr>
<td background="pic/bluebutton2.gif" width="80" align="center">
<span class="bluebut"><asp:LinkButton ID="lnkSearch" runat="server">
<img border="0" src="pic/ico-search.gif" width="16" height="16" align="absmiddle" style="margin-right:3px">Search</asp:LinkButton></span></td>
<td width="5px"></td>
<td background="pic/bluebutton2.gif" width="80" align="center">
<span class="bluebut"><a href="../preview_Index.aspx" target="_blank">
<img border="0" src="pic/ico-search.gif" width="16" height="16" align="absmiddle" style="margin-right:3px">Preview</a></span></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
                 <tr>
    <td style="padding:10px">
    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="table162">
<tr><td >
<telerik:RadToolTipManager ID="RadToolTipManager2" runat="server" ShowEvent="OnClick" HideEvent="ManualClose" OffsetY="0" Position="BottomCenter" RelativeTo="Element" Width="610px"/>
<telerik:RadGrid ID="RadGrid1" runat="server" AllowPaging="true"  AllowSorting="false"
AutoGenerateColumns="False" BorderWidth="1px" CellPadding="0" CssClass="con11" 
AllowMultiRowSelection="true" GridLines="None" GroupingEnabled="False" 
ShowStatusBar="True" Skin="Vista" PageSize="15" >
                                    
<AlternatingItemStyle BackColor="#F2F9F9" CssClass="con11" />
<ItemStyle CssClass="con11" />
                                    
<PagerStyle Mode="NextPrevAndNumeric" PagerTextFormat="page: {4} &amp;nbsp;Page &lt;strong&gt;{0}&lt;/strong&gt; of &lt;strong&gt;{1}&lt;/strong&gt;, items &lt;strong&gt;{2}&lt;/strong&gt; to &lt;strong&gt;{3}&lt;/strong&gt; of &lt;strong&gt;{5}&lt;/strong&gt;."
Position="TopAndBottom" />
                                     
<MasterTableView Width ="100%">
<Columns >
<telerik:GridClientSelectColumn HeaderText="Select" UniqueName="Select" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"  HeaderStyle-Width="5%" ItemStyle-Width="5%" ItemStyle-VerticalAlign="Top">
</telerik:GridClientSelectColumn>

<telerik:GridTemplateColumn HeaderText ="No" UniqueName ="No" HeaderStyle-Width ="5%" ItemStyle-Width ="5%" Visible="false">
    <HeaderStyle HorizontalAlign="Center" />
    <ItemStyle HorizontalAlign="Left"  VerticalAlign="Top"/>
    <ItemTemplate>
        <asp:Label ID="lblAutoNo" runat="server" CssClass="con11" Text="0"></asp:Label>
    </ItemTemplate>
</telerik:GridTemplateColumn>
                                            
<telerik:GridBoundColumn AllowFiltering ="false" AllowSorting ="true" DataField ="RECID" Display ="false"
Groupable ="false" HeaderText ="RECID" Reorderable ="false" Resizable ="false" ShowSortIcon ="false" 
UniqueName ="RECID" Visible ="false" >
</telerik:GridBoundColumn>

<telerik:GridTemplateColumn HeaderText="Thumbnail" UniqueName="Thumbnail" >
    <ItemTemplate >
        <BIP:BetterImage runat="server" ID="bipImg"  ToolTip='<%# Eval("ProdName") %>' ImageUrl='<%# Eval("ImgLink") %>' MaxHeight="50" MaxWidth="100" BorderStyle="None" Quantize="true" CacheOutput="True" ResizingQuality="HighQuality" OutputQuality="100"/><br/>
     </ItemTemplate>
    <HeaderStyle HorizontalAlign="Center"  />
    <ItemStyle HorizontalAlign="Center"  VerticalAlign="Top" Width="10%"  />
</telerik:GridTemplateColumn>

<telerik:GridTemplateColumn HeaderText="Deal Title" UniqueName="DealTitle"  >
    <ItemTemplate >
     <a href="#"><%--<asp:Label ID="lblProdName" runat="server" ToolTip='<%# Eval("ProdName") %>'><%# Eval("ProdNameLink")%></asp:Label>--%>
        <asp:Label ID="lblProdName" runat="server" ><%# Eval("ProdNameLink")%></asp:Label>
     </a>
    </ItemTemplate>
    <HeaderStyle HorizontalAlign="Center"  />
    <ItemStyle HorizontalAlign="Left"  VerticalAlign="Top" Width="20%"  />
</telerik:GridTemplateColumn>

<telerik:GridBoundColumn HeaderText ="Deal Code" DataField ="ProdCode" UniqueName ="DealCode">
    <HeaderStyle HorizontalAlign="center" Width="8%"  />
    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="8%"   />
</telerik:GridBoundColumn>
                                            
<telerik:GridBoundColumn HeaderText ="Start Date" DataField ="StartDate" UniqueName ="StartDate" DataFormatString="{0:dd/MM/yyyy <br/>hh:mm:ss tt}">
    <HeaderStyle HorizontalAlign="Center" Width="10%"  />
    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="10%"   />
</telerik:GridBoundColumn>

<telerik:GridBoundColumn HeaderText ="End Date" DataField ="EndDate" UniqueName ="EndDate" DataFormatString="{0:dd/MM/yyyy <br/>hh:mm:ss tt}" >
    <HeaderStyle HorizontalAlign="Center" Width="10%" />
    <ItemStyle HorizontalAlign="Center"  VerticalAlign="Top" Width="10%"   />
</telerik:GridBoundColumn>

<telerik:GridBoundColumn HeaderText ="Merchant" DataField ="ProdShortName" UniqueName ="MerchName">
    <HeaderStyle HorizontalAlign="Center"  />
    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
</telerik:GridBoundColumn>

<telerik:GridBoundColumn HeaderText ="Category" DataField ="Category" UniqueName ="Category">
    <HeaderStyle HorizontalAlign="Center"  />
    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
</telerik:GridBoundColumn>

<telerik:GridBoundColumn HeaderText ="Promotion Type" DataField ="PromotionType" UniqueName ="PromotionType" Visible="false">
    <HeaderStyle HorizontalAlign="Center"  />
    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
</telerik:GridBoundColumn>

<telerik:GridBoundColumn HeaderText ="Normal Price (RM)" DataField ="Price_RRP" UniqueName ="NormalPrice" Visible="false">
    <HeaderStyle HorizontalAlign="Center"  />
    <ItemStyle HorizontalAlign="right" VerticalAlign="Top" />
</telerik:GridBoundColumn>
                  
<telerik:GridBoundColumn HeaderText ="Discount Price (RM)" DataField ="Price_Discount" UniqueName ="DiscountPrice" Visible="false">
    <HeaderStyle HorizontalAlign="Center"  />
    <ItemStyle HorizontalAlign="right" VerticalAlign="Top" />
</telerik:GridBoundColumn>

<telerik:GridBoundColumn HeaderText ="Cash Voucher (RM)" DataField ="CashVoucher" UniqueName ="CashVoucher" Visible="false"> 
    <HeaderStyle HorizontalAlign="Center"  />
    <ItemStyle HorizontalAlign="right" VerticalAlign="Top" />
</telerik:GridBoundColumn>

<telerik:GridBoundColumn HeaderText ="Discount Voucher (%)" DataField ="DiscountVoucher" UniqueName ="DiscountVoucher" Visible="false">
    <HeaderStyle HorizontalAlign="Center"  />
    <ItemStyle HorizontalAlign="right" VerticalAlign="Top" />
</telerik:GridBoundColumn>

<telerik:GridBoundColumn HeaderText ="Credit" DataField ="Credit" UniqueName ="Credit">
    <HeaderStyle HorizontalAlign="Center"  />
    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top"  />
</telerik:GridBoundColumn>   

<telerik:GridBoundColumn HeaderText ="Total Qty" DataField ="Qty" UniqueName ="TotalQty">
    <HeaderStyle HorizontalAlign="Center"  />
    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top"  />
</telerik:GridBoundColumn>                         
 
 <telerik:GridBoundColumn HeaderText ="Total Sold" DataField ="TotalBought" UniqueName ="TotalBought">
    <HeaderStyle HorizontalAlign="Center"  />
    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
</telerik:GridBoundColumn>  

 <telerik:GridBoundColumn HeaderText ="Group" DataField ="GroupCode" UniqueName ="Group">
    <HeaderStyle HorizontalAlign="Center"  />
    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
</telerik:GridBoundColumn>  

 <telerik:GridBoundColumn HeaderText ="Status" DataField ="Status" UniqueName ="Status">
    <HeaderStyle HorizontalAlign="Center"  />
    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
</telerik:GridBoundColumn>  
                  
<telerik:GridTemplateColumn HeaderText="Action" UniqueName="Action" >
    <ItemTemplate >
         <a href="../Preview_Details.aspx?id=<%# Eval("RECID") %>" target="_Blank"><img alt="" src="pic/ico-preview.gif" border="0"  /></a>
        <a href="Product_Details.aspx?id=<%# Eval("RECID") %>"><img alt="" src="pic/ico-edit-xs.gif" border="0"  /></a>&nbsp;
        <asp:ImageButton ID="btnDel" ImageUrl="pic/ico-delete.gif" BorderStyle="None" CommandArgument='<%# Convert.ToString(Eval("RECID")) %>' CommandName="DEL" runat="server" OnClientClick="return confirm('Are you sure to Delete?');" />&nbsp;
        </ItemTemplate>
    <HeaderStyle HorizontalAlign="Center" Width="10%" />
    <ItemStyle HorizontalAlign="Center" Width="10%" VerticalAlign="Top" />
</telerik:GridTemplateColumn>
</Columns>
</MasterTableView>
                                     
<SelectedItemStyle BackColor="#F2F9F9" />
<ClientSettings AllowColumnsReorder="false" ReorderColumnsOnClient="false" ><Selecting AllowRowSelect="true" /></ClientSettings> 
<ActiveItemStyle BackColor="#F2F9F9" />
</telerik:RadGrid>
</td></tr>
<tr><td align="left" style="padding:5px;">
<table border="0" cellpadding="0" cellspacing="0" id="table1" height="26">
<tr>
<td background="pic/bluebutton3.gif" width="100" align="center">
<span class="bluebut"><asp:LinkButton ID="lnkDraft" runat="server" OnClientClick ="return confirm('Are you confirm to change status of selected deals?')">
<img border="0" src="pic/ico-draft.gif" width="16" height="16" align="absmiddle" style="margin-right:3px">Save as Draft</asp:LinkButton></span></td>
<td width="5px"></td>
<td background="pic/bluebutton3.gif" width="100" align="center">
<span class="bluebut"><asp:LinkButton ID="lnkPublish" runat="server" OnClientClick ="return confirm('Are you confirm to publish selected deals?')">
<img border="0" src="pic/ico-publish.gif" width="16" height="16" align="absmiddle" style="margin-right:3px">Publish Deal</asp:LinkButton></span></td>
</tr>
</table>
</td></tr>
</table>
    </td>
</tr>
                </table>
            </td>
        </tr>
     </table>
</td>
</tr>
</table>
</asp:Content>