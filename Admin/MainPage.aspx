﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="MainPage.aspx.vb" Inherits="Admin_MainPage" %>


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <link href="js/Class.css" rel="Stylesheet" type="text/css" />
    <title></title>
    <script type="text/javascript">

        function rowSelected(sender, args) {
            var grid = $find("<%#RadGrid1.ClientID %>");



            var MasterTable = grid.get_masterTableView();


            var dataItems = MasterTable.get_dataItems();

            var order = document.getElementById("order1");

            var selectedRows = MasterTable.get_selectedItems();


            var row = document.getElementById("RadGrid1_ctl00_ctl02_ctl00_lnkEmail");
            row.innerText = "(" + selectedRows.length + ")";
            console.log(row);



            var array = [];
            for (var i = 0; i < selectedRows.length; i++) {
                var row = selectedRows[i];


                var cell = MasterTable.getCellByColumnUniqueName(row, "OrderID");
                console.log(cell.innerText);
                console.log(array.push(cell.innerText));


                console.log(array);
                order.value = array.join('|||');
            }


        }

        function rowDeselected(sender, args) {
            var grid = $find("<%#RadGrid1.ClientID %>");
            var MasterTable = grid.get_masterTableView();
            console.log(args);
            var row = document.getElementById("RadGrid1_ctl00_ctl02_ctl00_lnkEmail");

            var selectedRows = MasterTable.get_selectedItems();



            row.innerText = "(" + selectedRows.length + ")";
            for (var i = 0; i < selectedRows.length; i++) {
                var row = selectedRows[i];
                var cell = MasterTable.getCellByColumnUniqueName(row, "OrderID")

                console.log(cell.innerText);


            }
        }

        function exportPDF() {


            var dummyContainer = document.getElementById('bodyTable');

            if (dummyContainer != null) {

                dummyContainer.style.display = "block";
                $find("<%#RadClientExportManager1.ClientID%>").exportPDF(dummyContainer);

            } else {

            }



        }


        (function () {
            var demo = window.demo = {};
            var grid;


            demo.GridCreated = function (sender) {
                grid = sender;
            };

            demo.HierarchyExpanded = function (sender, args) {

                var index = args.get_itemIndexHierarchical()

                console.log(args.get_itemIndexHierarchical());

                var row = args.get_tableView().get_dataItems()[index].get_element().nextSibling;
                var totalFare = $telerik.findElement(row, "lblTotalFare");
                var paymentMethod = $telerik.findElement(row, "lblPaymentType");
                var bookingDate = $telerik.findElement(row, "lblCreateDate");
                var toll = $telerik.findElement(row, "lblTotalToll");
                var taxiNo = $telerik.findElement(row, "lblTaxiNo");
                var taxiDriver = $telerik.findElement(row, "lblTaxiDriver");
                var distance = $telerik.findElement(row, "lblDistance");
                var addFrom = $telerik.findElement(row, "lblAddFrom");
                var addTo = $telerik.findElement(row, "lblAddTo");
                var orderID = $telerik.findElement(row, "lblOrderID");
                var taxiType = $telerik.findElement(row, "lblTaxiType");
                var taxiProductType = $telerik.findElement(row, "lblTaxiProductType");
                var field1 = $telerik.findElement(row, "lblField1");


                var rtotalFare = document.getElementById("receiptTotalFare");
                var rtotalFare1 = document.getElementById("receiptFare");
                var rtotalFare2 = document.getElementById("receiptTotalFareMain");
                var rpaymentMethod = document.getElementById("receiptPaymentType");
                var rbookingDate = document.getElementById("receiptBookingDate");
                var rtoll = document.getElementById("receiptToll");
                var rtaxiNo = document.getElementById("receiptTaxiNo");
                var rtaxiDriver = document.getElementById("receiptDriverName");
                var rdistance = document.getElementById("receiptDistance");
                var raddFrom = document.getElementById("receiptAddFrom");
                var raddTo = document.getElementById("receiptAddTo");
                var rOrderID = document.getElementById("receiptOrderID");
                var taxiProduct = document.getElementById("receiptTaxiProductType");
                var carType = document.getElementById("receiptTaxiType");

                carType = taxiType.innerText;
                taxiProduct = taxiProductType.innerText;
                rtotalFare.innerText = totalFare.innerText;
                rtotalFare1.innerText = totalFare.innerText;
                rtotalFare2.innerText = totalFare.innerText;
                rpaymentMethod.innerText = paymentMethod.innerText;
                rbookingDate.innerText = bookingDate.innerText;
                rtoll.innerText = toll.innerText;
                rtaxiNo.innerText = taxiNo.innerText;
                rtaxiDriver.innerText = taxiDriver.innerText;
                rdistance.innerText = distance.innerText;
                raddFrom.innerText = addFrom.innerText;
                raddTo.innerText = addTo.innerText;
                rOrderID.innerText = orderID.innerText;





            }

            demo.HierarchyCollapsed = function (sender, args) {
                var firstClientDataKeyName = args.get_tableView().get_clientDataKeyNames()[0];
            }

            demo.ExpandCollapseFirstMasterTableViewItem = function () {
                var firstMasterTableViewRow = grid.get_masterTableView().get_dataItems()[0];
                if (firstMasterTableViewRow.get_expanded()) {
                    firstMasterTableViewRow.set_expanded(false);
                }
                else {
                    firstMasterTableViewRow.set_expanded(true);
                }
            }
        })();

        function bookingChangePage() {

            var bookingpage = document.getElementById('bookingPage');
            var profilepage = document.getElementById('profilePage');
            var bookingtext = document.getElementById('mybooking');
            var bookingimg = document.getElementById('imgBooking');
            var profiletext = document.getElementById('profile');
            var profileimg = document.getElementById('imgProfile');

            bookingpage.style.display = "block";
            profilepage.style.display = "none";
            bookingtext.style.color = "#1191d0";
            bookingimg.src = "pic/my_booking_hover.png";
            profiletext.style.color = "#333333";
            profileimg.src = "pic/profile_ori.png";
        }

        function profileChangePage() {
            var bookingpage = document.getElementById('bookingPage');
            var profilepage = document.getElementById('profilePage');
            var bookingtext = document.getElementById('mybooking');
            var bookingimg = document.getElementById('imgBooking');
            var profiletext = document.getElementById('profile');
            var profileimg = document.getElementById('imgProfile');

            bookingpage.style.display = "none";
            profilepage.style.display = "block";
            bookingtext.style.color = "#333333";
            bookingimg.src = "pic/my_booking_ori.png";
            profiletext.style.color = "#1191d0";
            profileimg.src = "pic/profile_hover.png";
        }

        function showImageOnSelectedItemChanging(sender, eventArgs) {
            var input = sender.get_inputDomElement();
            input.style.background = "url(" + eventArgs.get_item().get_imageUrl() + ") no-repeat left center";
            input.style.paddingLeft = "30px";
            input.style.paddingBottom = "2px";

        }

        function OnClientLoadHandler(sender) {
            var input = sender.get_inputDomElement()
            var selectedItem = sender.get_selectedItem();
            input.style.background = "url(" + selectedItem.get_imageUrl() + ") no-repeat left center";
            input.style.paddingLeft = "30px";
            input.style.paddingBottom = "2px";



        }


        window.onload = function () {
            var grid = $find('<%# RadGrid1.ClientID %>');
            var masterTableView = grid.get_masterTableView();
            var totalbookings = document.getElementById('lblTotalBookings');
            var lblPage = document.getElementById('lblPage');
            var pageIndex = masterTableView.get_currentPageIndex();

            var pageCount = masterTableView.get_pageCount();
            var nextBtnBlue = document.getElementById('nextBtnBlue');
            var nextBtn = document.getElementById('nextBtn');
            var previousBtnBlue = document.getElementById('previousBtnBlue');
            var now = pageIndex + 1;

            var columns = masterTableView.get_dataItems();

            // alerts "ID" and "Name"
            console.log(columns.length);

            var totalbookingsreplace = totalbookings.innerText.replace(/\D/g, '');
            lblPage.innerText = "Showing 1 - " + columns.length + " of " + totalbookingsreplace;

            if (pageCount == now) {
                nextBtnBlue.style.display = 'none';


            }

        }



    </script>


</head>
<body>


    <center>

    <table style="display:none;"  align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;height: 100%;margin: 0;padding: 0;width: 100%;background-color: #FAFAFA;">
                <tbody>
                 <tr>
                    <td align="center" valign="top" id="bodyCell" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;height: 100%;margin: 0;padding: 10px;width: 100%;border-top: 0;">
           
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;border: 0;max-width: 600px !important;">
                            <tbody><tr>
                                <td valign="top" id="templatePreheader" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FFFFFF;border-top: 0;border-bottom: 0;padding-top: 9px;padding-bottom: 9px;"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner" style="padding-top: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
             
                <table border="0" cellpadding="0" cellspacing="0" style="text-align: center; max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" width="100%" class="mcnTextContentContainer">
                    <tbody><tr>
                        <td valign="top" class="mcnTextContent" style="padding: 0px 18px;line-height: 100%;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word">
							<img src="http://175.139.150.94/ezcab/ezpassenger/templates/pic/ezcab_logo.png" width="117px" height="54px" style="margin: auto; margin-top:-5px; display: block" />						
                        </td>
                    </tr>
                </tbody></table>
			
            </td>
        </tr>
    </tbody>
</table></td>
                            </tr>
                            <tr>
                                <td valign="top" id="templateHeader" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #ffffff;border-top: 0;border-bottom: 0;padding-top: 0px;padding-bottom: 0;"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner" style="padding-top: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
             
                <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" width="100%" class="mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="top" class="mcnTextContent" style="padding: 0px 18px;font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, Verdana, sans-serif;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #000000;font-size: 16px;line-height: 150%;text-align: left;">
                        
                            <h1 style="display: block;margin: 0;padding: 0;color: #202020;font-family: Helvetica;font-size: 26px;font-style: normal;font-weight: bold;line-height: 125%;letter-spacing: normal;text-align: left;"><span style="font-size:10pt">Thank you for choosing EzCab!</span></h1>

                        </td>
                    </tr>
                </tbody></table>
			
            </td>
        </tr>
    </tbody>
</table>

	<table class="bodyReceipt" style="font-family:Helvetica, Arial; margin-top: 5px" width="564px%">
	<tr  width="282px">
	<td style="padding-left: 18px; padding-bottom: 5px; color: #a5a7aa; font-size:7pt;">Total</td>
		<td style="color: #a5a7aa; font-size:7pt;">Date</td>
	</tr>
	<tr  width="282px">
		<td style="padding-left: 18px; font-size: 11pt; font-weight:bold;"><span>RM </span><span id="receiptTotalFare"></span></td>
		<td style="font-size: 11pt; font-weight:bold;"><span id="receiptBookingDate"></span></td>
	</tr>
	</table>

                            <tr>
                                <td valign="top" id="templateBody" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #ffffff;border-top: 0;border-bottom: 0;padding-top: 0;padding-bottom: 10px;"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;table-layout: fixed !important;">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width: 100%;padding: 10px 18px 5px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top-width: 1px;border-top-style: solid;border-top-color: #EAEAEA;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                    <tbody><tr>
                        <td style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>

            </td>
        </tr>
    </tbody>	
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
           
				<table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" width="100%" class="mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="top" class="mcnTextContent" style="padding: 0px 18px;font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, Verdana, sans-serif;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #202020;font-size: 16px;line-height: 150%;text-align: left;">
                        
                            <h1 style="display: block;margin: 0;padding: 0;color: #202020;font-family: Helvetica;font-size: 26px;font-style: normal;font-weight: bold;line-height: 125%;letter-spacing: normal;text-align: left;"><span style="font-size:10pt">Receipt Summary</span></h1>

                        </td>
                    </tr>					
                </tbody></table>
				<table  style="font-family:Helvetica, Arial; margin-left: 18px; margin-bottom: 5px" width="94%" cellspacing="0" cellpadding="0">
					<tr >
						<td colspan="2" style="padding:10px 0px 10px 18px; border-color:#a5a7aa; border: 1px solid; font-size: 8pt">Payment Method : <span id="receiptPaymentType" style="font-weight:bold; font-size: 9pt"></span></td>
					</tr>
					<tr>
						<td style="padding:10px 18px; border-color:#000000 ;border-left: 1px solid;border-bottom: 1px solid; font-size: 8pt"><span style="color:#a5a7aa">Description</span></td>
						<td style="padding:10px 18px; border-color:#000000 ;border-right: 1px solid;border-bottom: 1px solid;text-align: right; font-size: 8pt"><span style="color:#a5a7aa">Amount</span></td>
					</tr>
					<tr style="border-bottom-style:none">
						<td style="padding:10px 18px; border-color:#000000 ;border-left: 1px solid; font-size: 8pt">Fare</td>
						<td style="padding:10px 18px; border-color:#000000 ;border-right: 1px solid; text-align: right; font-size: 9pt"><span id="receiptFare"></span></td>
					</tr>
					<tr style="border-bottom-style:none">
						<td style="padding:0px 18px; border-color:#000000 ;border-left: 1px solid; font-size: 8pt">Toll</td>
						<td style="padding:0px 18px; border-color:#000000 ;border-right: 1px solid; text-align: right; font-size: 9pt"><span id="receiptToll"></span></td>
					</tr>
					<tr>
						<td style="padding:10px 18px; border-color:#000000 ;border-left: 1px solid; font-size: 8pt">Promo Code  - <span id="receiptPromoCode"></span></td>
						<td style="padding:10px 18px; border-color:#000000 ;border-right: 1px solid;text-align: right; font-size: 9pt"><span id="receiptDiscount"></span></td>
					</tr>
					<tr >
						<td style="padding:10px 18px; border-color:#000000 ;border-left: 1px solid;border-top: 1px solid;border-bottom: 1px solid; font-weight: bold; font-size: 8pt">Total</td>
						<td style="padding:10px 18px; border-color:#000000 ;border-right: 1px solid;border-top: 1px solid;border-bottom: 1px solid; font-weight: bold;  text-align: right; font-size: 9pt"><span id="receiptTotalFareMain"></span></td>
					</tr>
				
				</table>
				
                <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" width="100%" class="mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="top" class="mcnTextContent" style="padding: 0px 18px 5px;font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, Verdana, sans-serif;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #202020;font-size: 16px;line-height: 150%;text-align: left;">
                        
                            <h1 style="display: block;margin: 0;padding: 0;color: #202020;font-family: Helvetica;font-size: 26px;font-style: normal;font-weight: bold;line-height: 125%;letter-spacing: normal;text-align: left;"><span style="font-size:10pt">Booking Details</span></h1>

                        </td>
                    </tr>					
                </tbody></table>
				<table class="bodyReceipt" style="font-family:Helvetica, Arial; margin-left: 18px;" width="94%">
					<tr>
						<td style="padding-bottom: 2px; color: #a5a7aa; font-size:7pt">Driver Name</td>
						<td style="padding:0px 0px 2px 18px; color: #a5a7aa; font-size:7pt">Order ID</td>
					</tr>
					<tr>
						<td style="padding-bottom: 10px; padding-right: 18px; table-layout:fixed; width:299px; word-break:break-all; font-size:8pt"><span id="receiptDriverName"></span>#</td>
						<td style="padding:0px 0px 10px 18px; vertical-align:text-top; font-size:8pt"><span id="receiptOrderID"></span></td>
					</tr>
					
					<tr>
						<td style="padding-bottom: 2px; color: #a5a7aa; font-size:7pt">Taxi No</td>
						<td style="padding:0px 0px 2px 18px; color: #a5a7aa; font-size:7pt">Vehicle Type</td>
					</tr>
					<tr>
						<td style="padding-bottom: 10px; font-size:8pt"><span id="receiptTaxiNo"></span></td>
						<td style="padding:0px 0px 10px 18px; font-size:8pt"><span id="receiptTaxiProductType"></span></td>
					</tr>
					
					<tr>
					<td style="padding-bottom: 2px; color: #a5a7aa; font-size:7pt">Distance</td>
					<td style="padding:0px 0px 2px 18px; color: #a5a7aa; font-size:7pt">Car Model</td>
					</tr>
					
					<tr>
						<td style="padding-bottom: 10px; font-size:8pt"><span id="receiptDistance"></span> <span>KM</span></td>
						<td style="padding:0px 0px 10px 18px; font-size:8pt"><span id="receiptTaxiType"></span></td>
					</tr>
					
					<tr>
						<td style="padding-bottom: 2px; color: #a5a7aa; font-size:7pt">From</td>
					</tr>
					
					<tr>
						<td colspan="2" style="padding-bottom: 10px; font-size:8pt; text-decoration: none"><span id="receiptAddFrom"></span></td>
					</tr>
					
					<tr>
						<td style="padding-bottom: 2px; color: #a5a7aa; font-size:7pt">To</td>
					</tr>
					
					<tr>
						<td colspan="2" style="padding-bottom: 10px; font-size:8pt; text-decoration: none"><span id="receiptAddTo"></span></td>
					</tr>				
				</table>
			
            </td>
        </tr>
    </tbody>
</table></td>
                            </tr>
                            <tr>
                                <td valign="top" id="templateFooter" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #1d90d0;border-top: 0;border-bottom: 0;padding: 0px 0px"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
    <tbody class="mcnFollowBlockOuter">
        <tr>
            <td align="center" valign="top" style="padding: 0px 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnFollowBlockInner">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentContainer" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
    <tbody><tr>
	<td align="left" style="padding-left: 9px;padding-right: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;
	color: white; font-size: 7pt; font-family:Helvetica, Arial; text-decoration: none">
	Customer Support : +603-6259 1913
	</td>
        <td align="right" style="padding-left: 9px;padding-right: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnFollowContent">
                <tbody><tr>
                    <td align="right" valign="top" style="padding-top: 9px;padding-right: 9px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                        <table align="right" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                            <tbody><tr>
                                <td align="right" valign="top" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                             
                                        
                                            <table align="left" border="0" cellpadding="0" cellspacing="0" style="display: inline;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                <tbody><tr>
                                                    <td valign="top" style="padding-right: 10px;padding-bottom: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnFollowContentItemContainer">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                            <tbody><tr>
                                                                <td align="left" valign="middle" style="padding-top: 5px;padding-right: 10px;padding-bottom: 5px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                        <tbody><tr>
                                                                            
                                                                                <td align="center" valign="middle" width="24" class="mcnFollowIconContent" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                    <a href="https://www.facebook.com/ezcabmalaysia" "_blank"="" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" target="_blank"><img src="http://175.139.150.94/ezcab/ezpassenger/templates/pic/facebook_icon.png" style="display: block;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" height="25" width="25" class="" /></a>
                                                                                </td>
                                                                            
                                                                            
                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        
                              
                                            <table align="left" border="0" cellpadding="0" cellspacing="0" style="display: inline;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                <tbody><tr>
                                                    <td valign="top" style="padding-right: 10px;padding-bottom: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnFollowContentItemContainer">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                            <tbody><tr>
                                                                <td align="left" valign="middle" style="padding-top: 5px;padding-right: 10px;padding-bottom: 5px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                        <tbody><tr>
                                                                            
                                                                                <td align="center" valign="middle" width="24" class="mcnFollowIconContent" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                    <a href="http://www.ezcab.com.my/" "_blank"="" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" target="_blank"><img src="http://175.139.150.94/ezcab/ezpassenger/templates/pic/link_icon.png" style="display: block;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" height="25" width="25" class="" /></a>
                                                                                </td>
                                                                            
                                                                            
                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                            
                                        
                                            <table align="left" border="0" cellpadding="0" cellspacing="0" style="display: inline;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                <tbody><tr>
                                                    <td valign="top" style="padding-right: 0;padding-bottom: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnFollowContentItemContainer">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                            <tbody><tr>
                                                                <td align="left" valign="middle" style="padding-top: 5px;padding-right: 10px;padding-bottom: 5px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                        <tbody><tr>
                                                                            
                                                                                <td align="center" valign="middle" width="24" class="mcnFollowIconContent" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                    <a href="mailto:enquiry@ezcab.com.my" "_blank"="" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" target="_blank"><img src="http://175.139.150.94/ezcab/ezpassenger/templates/pic/message_icon.png" style="display: block;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" height="25" width="25" class="" /></a>
                                                                                </td>
                                                                            
                                                                            
                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        
                                   
                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
            </tbody></table>
        </td>
    </tr>
</tbody></table>

            </td>
        </tr>
    </tbody>
</table></td>
                            </tr>
                        </tbody></table>
				
                    </td>
                </tr>
            </tbody>
            </table>

        </center>
    <form runat="server">
        <asp:HiddenField ID="order1" runat="server" />
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <telerik:RadAjaxManagerProxy runat="server" ID="RadAjaxManagerProxy1">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="lnkEmail">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="lnkEmail" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManagerProxy>

        <table width="100%">
            <tr>
                <td style="padding-left: 90px; padding-top: 30px; padding-bottom: 20px;">

                    <img src="pic/ezcab_home_logo.png" />
                </td>



                <td align="right" style="padding-right: 8%;">
                    <asp:LinkButton ID="lnkSignOut" CssClass="linkSignOut" runat="server" Text="Sign Out ">Sign Out</asp:LinkButton>

                </td>

            </tr>
        </table>

        <div id="orderDetails" runat="server"></div>


        <table runat="server" id="mainContainer" class="main-container" width="100%">
            <tr>

                <td valign="top" width="20%" style="border-right-style: solid; border-width: 1px; text-align: center; border-right-color: lightgray;">

                    <br />


                    <img id="imgAvatar" runat="server" class="imgAvatar" src='<%# Eval("ProfilePicture") %>' width="40" height="40" />
                    <br />



                    <asp:Label CssClass="usrName" runat="server" ID="lblName" Text="john king"></asp:Label>

                    <br />
                    <br />
                    <br />




                    <a href="#form" id="target1" style="text-decoration: none; vertical-align: middle; padding-right: 30px;" class="target" onclick="bookingChangePage()">
                        <img id="imgBooking" src="pic/my_booking_hover.png" style="vertical-align: middle;" class="image"
                            width="24" height="24" />
                        <span id="mybooking" style="line-height: 24px; color: #1d90d0">My Booking</span>
                    </a>
                    <br />
                    <br />

                    <a href="#form" id="target2" style="text-decoration: none; padding-right: 70px;" class="target" onclick="profileChangePage()">
                        <img id="imgProfile" src="pic/profile_ori.png" style="vertical-align: middle;" class="image"
                            width="24" height="24" />
                        <span id="profile" style="line-height: 24px">Profile</span>
                    </a>



                    <br />


                </td>

                <td id="bookingPage" valign="top" style="background-color: #f8f8f8; height: 100%; padding-bottom: 20px; border-left-color: lightgray;">




                    <div style="padding-left: 40px;" class="mainTitle">My Booking</div>
                    <br />

                    <table>

                        <tr>
                            <td class="coll12" style="padding-left: 40px;">From
                                    
                            </td>

                            <td class="coll12" style="padding-left: 40px;">To
                                    
                            </td>

                            <td class="coll12" style="padding-left: 40px;">View
                                    
                            </td>

                        </tr>

                        <tr>

                            <td style="padding-left: 40px;">

                                <br />
                                <telerik:RadDatePicker runat="server" ID="rdpFrom">
                                    <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy"></DateInput>
                                </telerik:RadDatePicker>


                            </td>

                            <td style="padding-left: 40px;">
                                <br />
                                <telerik:RadDatePicker runat="server" ID="rdpTo">
                                    <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy"></DateInput>
                                </telerik:RadDatePicker>

                            </td>

                            <td style="padding-left: 40px;">
                                <br />
                                <asp:DropDownList runat="server" ID="ddlStatus">
                                    <asp:ListItem Text="ALL" Value=""></asp:ListItem>
                                    <asp:ListItem Text="Business" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Personal" Value="0"></asp:ListItem>
                                </asp:DropDownList>

                            </td>


                            <td style="padding-left: 100px;">
                                <br />
                                <asp:LinkButton runat="server" ID="lnkSearch"><img src="pic/search_ori.png"/></asp:LinkButton>

                            </td>



                        </tr>


                    </table>




                    <table width="100%">


                        <tr>
                            <td>




                                <asp:Panel ID="Panel1" Width="100%" runat="server">
                                    <telerik:RadMultiPage runat="server" ID="RadMultiPage1" SelectedIndex="0">
                                        <telerik:RadPageView runat="server" ID="RadPageView2">
                                            <asp:Panel runat="server" ID="pnlRadPageView2">


                                                <telerik:RadClientExportManager ID="RadClientExportManager1" runat="server">
                                                    <PdfSettings PaperSize="A4" MarginBottom="12mm" FileName="Ticket.pdf" />

                                                </telerik:RadClientExportManager>

                                                <telerik:RadGrid PagerStyle-Position="Top" BackColor="#f8f8f8" MasterTableView-ShowHeadersWhenNoRecords="false" PagerStyle-AlwaysVisible="true" PageSize="13" PagerStyle-PagerTextFormat=" {4} Showing {0} - {1} of {5}" PagerStyle-BorderStyle="None" PagerStyle-BackColor="#f8f8f8" PagerStyle-Mode="NextPrev" runat="server" ID="RadGrid1" AutoGenerateColumns="False" Width="90%"
                                                    AllowPaging="True"
                                                    CellPadding="0" CellSpacing="0" OnDetailTableDataBind="RadGrid1_DetailTableDataBind" OnNeedDataSource="RadGrid1_NeedDataSource" GridLines="None" ShowStatusBar="True"
                                                    CssClass="radGrid" Skin="Silk" ClientSettings-Resizing-AllowColumnResize="false" AllowSorting="true" ClientSettings-Selecting-AllowRowSelect="true" AllowMultiRowSelection="true">
                                                    <AlternatingItemStyle BackColor="White" CssClass="con11" />
                                                    <ItemStyle CssClass="con11" />
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ClientSettings Selecting-AllowRowSelect="true">
                                                        <ClientEvents OnGridCreated="demo.GridCreated" OnRowSelected="rowSelected"
                                                            OnRowDeselected="rowDeselected" OnHierarchyExpanded="demo.HierarchyExpanded" OnHierarchyCollapsed="demo.HierarchyCollapsed"></ClientEvents>
                                                    </ClientSettings>
                                                    <ExportSettings IgnorePaging="true">
                                                        <Pdf PageTitle="My Page" PaperSize="A4" />
                                                    </ExportSettings>
                                                    <MasterTableView CssClass="masterTable" DataKeyNames="RECID" Width="100%" HierarchyLoadMode="Client">

                                                        <NoRecordsTemplate>
                                                            <div class="noBookings" style="height: 220px; text-align: center; padding-top: 50px; margin-top: 50px;">
                                                                No Bookings To Be Shown
                                                            </div>

                                                        </NoRecordsTemplate>

                                                        <PagerTemplate>



                                                            <table border="0" id="table1" cellpadding="5" width="100%" style="border-collapse: collapse;">

                                                                <tr>

                                                                    <td colspan="5">
                                                                        <hr style="border-color: #e3e7e9" />
                                                                    </td>

                                                                </tr>

                                                                <tr>
                                                                    <td>
                                                                        <span id="lblTotalBookings" class="col13"><%# "Total" + " " + Container.OwnerTableView.PagingManager.DataSourceCount.ToString + " Bookings " %></span>



                                                                    </td>

                                                                    <td style="float: right; padding: 1%">
                                                                        <asp:ImageButton ID="ImageButton1" runat="server" OnClick="DownloadPDF_Click" ImageUrl="pic/download_pdf_ori.png" CssClass="pdfButton"></asp:ImageButton>

                                                                    </td>

                                                                    <td style="float: right;">
                                                                        <asp:Label runat="server" CssClass="col13" Text="Download PDF Statement"></asp:Label>

                                                                    </td>

                                                                    <td style="float: right;">
                                                                        <div style="background-color: white; padding: 0px 10px;">
                                                                            <img src="pic/email.png" style="padding-right: 10px;" />
                                                                            <asp:LinkButton ID="lnkEmail" Style="text-decoration: none;" CssClass="lnkEmailBtn" OnClick="Download_Receipt" runat="server"></asp:LinkButton>
                                                                        </div>

                                                                    </td>

                                                                    <td style="float: right;">

                                                                        <asp:Label runat="server" CssClass="col13" Text="Email Receipts"></asp:Label>

                                                                    </td>


                                                                </tr>

                                                                <tr>

                                                                    <td colspan="5">
                                                                        <hr />
                                                                    </td>

                                                                </tr>


                                                                <tr>



                                                                    <td style="border-style: none;">
                                                                        <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false"
                                                                            CommandArgument="Prev"><img id="previousBtnBlue" src="pic/ezcab_app_page_left_b.png"/></asp:LinkButton>
                                                                        <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false"
                                                                            CommandArgument="Next" OnClientClick="nextClick(); return false;"><img id="nextBtnBlue" style="padding-bottom:1px;"  src="pic/ezcab_app_page_right_b.png"/></asp:LinkButton>
                                                                        <span id="lblPage"></span>

                                                                    </td>

                                                                </tr>
                                                            </table>
                                                        </PagerTemplate>



                                                        <Columns>

                                                            <telerik:GridClientSelectColumn ItemStyle-ForeColor="Blue" UniqueName="Select" HeaderStyle-BackColor="Red" HeaderText="Select" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%" ItemStyle-Width="2%">
                                                            </telerik:GridClientSelectColumn>

                                                            <telerik:GridBoundColumn ItemStyle-CssClass="itemStyle" DataField="CreateDate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Date" UniqueName="CreateDate">
                                                                <HeaderStyle ForeColor="Black" HorizontalAlign="Left" Width="5%" />
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </telerik:GridBoundColumn>

                                                            <telerik:GridBoundColumn Display="false" ItemStyle-CssClass="itemStyle" DataField="OrderID" HeaderText="OrderID" UniqueName="OrderID">
                                                                <HeaderStyle HorizontalAlign="Left" Width="5%" />
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </telerik:GridBoundColumn>

                                                            <telerik:GridTemplateColumn Display="false" HeaderStyle-CssClass="headerStyle" HeaderText="No." UniqueName="No">
                                                                <HeaderStyle HorizontalAlign="Left" Width="2%" />
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAutoNo" runat="server" CssClass="con14"> </asp:Label>
                                                                </ItemTemplate>
                                                            </telerik:GridTemplateColumn>

                                                            <telerik:GridBoundColumn ItemStyle-CssClass="itemStyle" DataField="TotalFare" HeaderText="Fare" UniqueName="TotalFare">
                                                                <HeaderStyle CssClass="headerStyle" ForeColor="Black" HorizontalAlign="Left" Width="5%" />
                                                                <ItemStyle CssClass="itemStyle" HorizontalAlign="Left" />
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn ItemStyle-CssClass="itemStyle" DataField="PaymentType" HeaderText="Payment" UniqueName="PaymentType">
                                                                <HeaderStyle CssClass="headerStyle" ForeColor="Black" HorizontalAlign="Left" Width="5%" />
                                                                <ItemStyle CssClass="itemStyle" HorizontalAlign="Left" />
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn ItemStyle-CssClass="itemStyle" DataField="isBusiness" HeaderText="Tag" UniqueName="Address">
                                                                <HeaderStyle CssClass="headerStyle" ForeColor="Black" HorizontalAlign="Left" Width="3%" />
                                                                <ItemStyle CssClass="itemStyle" HorizontalAlign="Left" />

                                                            </telerik:GridBoundColumn>

                                                            <telerik:GridBoundColumn ItemStyle-CssClass="itemStyle" DataField="AddFromName" HeaderText="Pick Up" UniqueName="From">
                                                                <HeaderStyle CssClass="headerStyle" ForeColor="Black" HorizontalAlign="Left" Width="3%" />
                                                                <ItemStyle CssClass="itemStyle" Width="7%" HorizontalAlign="Left" />

                                                            </telerik:GridBoundColumn>

                                                            <telerik:GridBoundColumn ItemStyle-CssClass="itemStyle" DataField="AddToName" HeaderText="Drop Off" UniqueName="To">
                                                                <HeaderStyle CssClass="headerStyle" ForeColor="Black" HorizontalAlign="Left" Width="3%" />
                                                                <ItemStyle CssClass="itemStyle" Width="7%" HorizontalAlign="Left" />

                                                            </telerik:GridBoundColumn>



                                                            <telerik:GridExpandColumn ButtonType="ImageButton" UniqueName="ImageBtn" CollapseImageUrl="~/Admin/pic/booking_list_up.png" ExpandImageUrl="~/Admin/pic/booking_list_down.png">
                                                            </telerik:GridExpandColumn>

                                                        </Columns>

                                                        <DetailTables>
                                                            <telerik:GridTableView Width="98%" CssClass="MyGridView" AllowPaging="false" Name="Detail" HorizontalAlign="Justify" HierarchyLoadMode="Client">

                                                                <Columns>

                                                                    <telerik:GridTemplateColumn HeaderStyle-CssClass="itemTemplate" ItemStyle-CssClass="itemTemplate" UniqueName="TemplateColumn" SortExpression="CompanyName"
                                                                        InitializeTemplatesFirst="false">



                                                                        <ItemTemplate>
                                                                            <table style="width: 100%;">
                                                                                <colgroup>
                                                                                    <col style="height: 200px;" />
                                                                                    <col style="height: 200px;" />
                                                                                    <col style="height: 200px;" />
                                                                                    <col style="height: 200px;" />
                                                                                    <col style="height: 200px;" />


                                                                                </colgroup>

                                                                                <tr>
                                                                                    <td>
                                                                                        <br />
                                                                                    </td>

                                                                                </tr>

                                                                                <tr>
                                                                                    <td width="40%">
                                                                                        <b>Booking Detail</b>

                                                                                    </td>


                                                                                    <td class="mainTag" colspan="2">Order ID: <span id="lblOrderID" runat="server" class="detailTag"><%# Eval("OrderID") %></span>
                                                                                    </td>


                                                                                    <td class="mainTag" colspan="3">Your Rating:
                                                                                        <asp:Literal ID="ltrRating" runat="server"></asp:Literal>
                                                                                        <asp:HiddenField ID="hldRating" runat="server" Value='<%#Eval("Rate") %>' />
                                                                                    </td>



                                                                                </tr>

                                                                                <tr>

                                                                                    <td colspan="5">
                                                                                        <hr />
                                                                                    </td>

                                                                                </tr>

                                                                                <tr style="height: 20px">
                                                                                </tr>



                                                                                <tr>

                                                                                    <td class="mainTag" width="20%">Total Payment</td>
                                                                                    <td class="mainTag" width="20%">Total Fare</td>
                                                                                    <td class="mainTag" width="20%">Promo Code</td>
                                                                                    <td class="mainTag" width="20%">Payment Method

                                                                                        <asp:Label ID="lblTaxiType" runat="server" Text='<%# Eval("TaxiType") %>'></asp:Label>
                                                                                        <asp:Label ID="lblTaxiProductType" runat="server" Text='<%# Eval("TaxiProduct") %>'></asp:Label>
                                                                                    </td>

                                                                                    <td style="display: none;" class="detailTable" width="20%">
                                                                                        <asp:Label ID="RECID" runat="server" Text='<%# Eval("PromoCodeRECID") %>'></asp:Label></td>

                                                                                    
                                                                                    <td style="display: none;" class="detailTable" width="20%">
                                                                                        <asp:Label ID="lblRECID" runat="server" Text='<%# Eval("RECID") %>'></asp:Label></td>
                                                                                    
                                                                                     
                                                                                </tr>



                                                                                <tr>

                                                                                    <td class="detailTag" width="20%">

                                                                                        <asp:Label ID="lblTotalFare" runat="server" Text='<%# Eval("TotalFare") %>'></asp:Label>
                                                                                    </td>
                                                                                    <td class="detailTag" width="20%">

                                                                                        <asp:Label ID="lblTotalToll" runat="server" Text='<%# Eval("TotalToll") %>'></asp:Label>

                                                                                    <td class="detailTag" width="20%">
                                                                                        <asp:Label ID="promoCode" runat="server" Text='<%# Eval("Code") %>'></asp:Label>
                                                                                    </td>
                                                                                    <td class="detailTag" width="20%">
                                                                                        <asp:Label ID="lblPaymentType" runat="server" Text='<%# Eval("PaymentType") %>'></asp:Label></td>

                                                                                </tr>

                                                                                <tr style="height: 20px">
                                                                                </tr>


                                                                                <tr>

                                                                                    <td class="mainTag" width="20%">Date & Time</td>
                                                                                    <td class="mainTag" width="20%">Driver Name</td>
                                                                                    <td class="mainTag" width="20%">Vehicle No.</td>

                                                                                </tr>

                                                                                <tr>

                                                                                    <td class="detailTag" width="20%">
                                                                                        <asp:Label ID="lblCreateDate" runat="server" Text='<%# Eval("CreateDate", "{0:dd/MM/yyyy hh:mm ttt}") %>'></asp:Label></td>
                                                                                    <td class="detailTag" width="20%">
                                                                                        <asp:Label ID="lblTaxiDriver" runat="server" Text='<%# Eval("TaxiDriver") %>'></asp:Label></td>
                                                                                    <td class="detailTag" width="20%">
                                                                                        <asp:Label ID="lblTaxiNo" runat="server" Text='<%# Eval("TaxiNo") %>'></asp:Label></td>

                                                                                </tr>

                                                                                <tr style="height: 20px">
                                                                                </tr>


                                                                                <tr>

                                                                                    <td class="mainTag" width="20%">Tag</td>
                                                                                    <td class="mainTag" width="20%">Distance</td>

                                                                                </tr>
                                                                                <tr>

                                                                                    <td class="detailTag" width="20%">
                                                                                        <asp:Label ID="isBusiness" runat="server" Text='<%# Eval("IsBusiness") %>'></asp:Label>
                                                                                    </td>
                                                                                    <td class="detailTag" width="20%">
                                                                                        <asp:Label ID="lblDistance" runat="server" Text='<%# Eval("EstDistFromTo") %>'></asp:Label></td>

                                                                                </tr>

                                                                                <tr style="height: 30px">
                                                                                </tr>


                                                                                <tr>

                                                                                    <td class="mainTag" width="20%">From</td>

                                                                                </tr>

                                                                                <tr>

                                                                                    <td class="detailTag" width="20%">
                                                                                        <asp:Label ID="lblAddFrom" runat="server" Text='<%# Eval("AddFrom") %>'></asp:Label></td>

                                                                                </tr>

                                                                                <tr style="height: 30px">
                                                                                </tr>

                                                                                <tr>

                                                                                    <td class="mainTag" width="20%">To</td>


                                                                                </tr>

                                                                                <tr>

                                                                                    <td class="detailTag" width="20%">
                                                                                        <asp:Label ID="lblAddTo" runat="server" Text='<%# Eval("AddTo") %>'></asp:Label></td>

                                                                                    <td width="20%"></td>

                                                                                    <td width="20%"></td>

                                                                                    <td width="20%">
                                                                                        <asp:LinkButton ID="lnkBtnReceipt" runat="server" CommandName="download" OnClientClick="exportPDF();" CausesValidation="false"><img src="pic/download_receipt_ori.png"/></asp:LinkButton>

                                                                                    </td>


                                                                                </tr>

                                                                                <tr>


                                                                                    <td colspan="5">
                                                                                        <hr />
                                                                                    </td>

                                                                                </tr>

                                                                                <tr>
                                                                                    <td colspan="5"></td>

                                                                                </tr>

                                                                            </table>


                                                                        </ItemTemplate>

                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </telerik:GridTemplateColumn>

                                                                </Columns>

                                                            </telerik:GridTableView>
                                                        </DetailTables>
                                                    </MasterTableView>
                                                </telerik:RadGrid>
                                            </asp:Panel>

                                        </telerik:RadPageView>
                                    </telerik:RadMultiPage>

                                </asp:Panel>

                            </td>
                        </tr>


                    </table>

                </td>




                <td id="profilePage" valign="top" style="background-color: #f8f8f8; height: 500px; display: none; border-left-color: lightgray;">
                    <table>
                        <tr style="padding-left: 10px;">
                            <td>

                                <div class="mainTitle">Profile</div>

                            </td>


                        </tr>

                        <tr>

                            <td>
                                <br />
                            </td>

                        </tr>

                        <tr>
                            <td>
                                <div class="col14">Name</div>


                            </td>

                            <td>
                                <div id="profileName" runat="server" style="padding-left: 30px;" class="profileTxt"></div>


                            </td>


                        </tr>

                        <tr>

                            <td>
                                <br />
                            </td>

                        </tr>

                        <tr>
                            <td>

                                <div class="col14">Email</div>

                            </td>

                            <td>

                                <div id="profileEmail" runat="server" style="padding-left: 30px;" class="profileTxt"></div>
                                &nbsp<img src="pic/profile_email_info.png" />

                            </td>



                        </tr>

                        <tr>

                            <td>
                                <br />
                            </td>

                        </tr>


                        <tr style="padding-left: 30px;">
                            <td>

                                <div class="col14">Mobile No.</div>
                            </td>

                            <td>
                                <div id="profileMobileNo" runat="server" style="padding-left: 30px;" class="profileTxt"></div>


                            </td>


                        </tr>



                    </table>



                </td>



            </tr>


        </table>



        <table width="85%" style="margin-left: 90px;">
            <tr>

                <td style="padding-top: 20px;">
                    <div style="color: black" class="leftcol">
                        Terms Of Service | Privacy Policy
                    </div>
                    <div class="rightcol">
                        Copyright @ EzCab Sdn Bhd
                    </div>

                </td>



            </tr>

        </table>

    </form>

</body>
</html>
