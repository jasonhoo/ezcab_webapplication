﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Index.aspx.vb" Inherits="Admin_Index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <meta name="viewport" content="width=device-width,height=device-height,initial-scale=1.0"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous" />
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
 <link href="js/Class.css" rel="Stylesheet" type="text/css" />
    
    <title></title>

   
</head>
<body style="overflow: hidden;">
    <form class="mainForm" style="overflow: hidden;" runat="server">
        <div>

            <asp:ScriptManager ID="sc" runat="server">
            </asp:ScriptManager>
            <asp:UpdatePanel ID="up1" runat="server">
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="Timer1" EventName="Tick" />
                </Triggers>
                <ContentTemplate>
                    <asp:Timer ID="Timer1" runat="server" Interval="2000" OnTick="Timer1_Tick">
                    </asp:Timer>
                </ContentTemplate>
            </asp:UpdatePanel>


            <table class="table1" border="0" cellpadding="0" cellspacing="0">
                <tr>


                    <td class="backgroundImg">



                        <img src="pic/ezcab_login_logo.png" class="ezcabImg" />

                    </td>

                </tr>

                <tr>
                    <td class="welcome">Welcome

                    </td>


                </tr>

                <tr>
                    <td class="qrcode">SCAN QR CODE TO SIGN IN
                    </td>

                </tr>

                <tr>
                    <td>
                        <br />

                    </td>


                </tr>

                <tr>
                    <td id="qrcode">
                        <asp:Label ID="qrtext" Style="display: none;" runat="server"></asp:Label>

                    </td>

                </tr>

                <tr>
                    <td class="informationQRCode">Use EzCab on your phone to scan the app...

                    </td>


                </tr>


                <tr>
                    <td style="padding-top: 40px;"></td>


                </tr>



            </table>


            <div style="position: absolute; bottom: 0; left: 0; width: 100%;">
                <table style="width: 100%">
                    <tr>
                        <td style="color: black; padding-bottom: 20px;" align="bottom" class="leftcol">Terms Of Service | Privacy Policy

                        </td>

                        <td style="color: black; padding-bottom: 20px;" class="rightcol">Copyright @ EzCab Sdn Bhd

                        </td>

                    </tr>

                </table>

            </div>

        </div>
    </form>

    <script src="js/qrcode.min.js"> </script>

    <script type="text/javascript">
        var code = document.getElementById('<%= qrtext.ClientID %>')

        var qrcode = new QRCode("qrcode", {
            text: code.innerText,
            width: 200,
            height: 200,
            colorDark: "#000000",
            colorLight: "#ffffff",
            correctLevel: QRCode.CorrectLevel.H
        });

    </script>

</body>
</html>
