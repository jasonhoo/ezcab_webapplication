﻿Imports System.IO
Imports System.Data
Imports System.Configuration
Imports Telerik.Web.UI
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports System.Web
Partial Class Admin_Index
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        If IsPostBack = False Then

            loadUnqiueID()

        End If
    End Sub



    Private Sub loadUnqiueID()

        Dim id As Guid = Guid.NewGuid()
        qrtext.Text = "EZCAB" + id.ToString

        Dim strSQL As String = ""
        Dim objDB As Database
        Dim strTaxiNo As String = ""

        Dim blnShow_Dev_Err_Msg As Boolean = System.Configuration.ConfigurationManager.AppSettings("Show_Dev_Err_Msg")

        strSQL = "EXECUTE sp_Create_User_Web_Login "

        Try
            strSQL += "'" + qrtext.Text + "', NULL, 0 "

            objDB = DatabaseFactory.CreateDatabase()
            objDB.ExecuteNonQuery(CommandType.Text, strSQL)



        Catch ex As Exception
            If blnShow_Dev_Err_Msg = False Then
                Response.Write(ex.Message + " - FAIL to load driver.")
            End If
            If blnShow_Dev_Err_Msg = True Then
                Response.Write(ex.Message + " " + strSQL)
            End If
        Finally
            objDB = Nothing
        End Try


    End Sub


    Public Sub loadPassengerDetail()

        Dim strSQL As String = ""
        Dim objDB As Database
        Dim strnewid As String = ""
        Dim strTaxiNo As String = ""
        Dim strRedirect As String = ""
        Dim id As Guid = Guid.NewGuid()
        Dim objDS As DataSet
        Dim blnShow_Dev_Err_Msg As Boolean = System.Configuration.ConfigurationManager.AppSettings("Show_Dev_Err_Msg")



        Try
            strSQL = "SELECT MemberRECID, LoginCode FROM tblWebLogin WHERE LEN(MemberRECID) > 0 AND LoginCode = '" & CommonLib.SQLStrEncode(qrtext.Text) & "' "

            objDB = DatabaseFactory.CreateDatabase()
            objDS = objDB.ExecuteDataSet(CommandType.Text, strSQL)


            If objDS.Tables(0).Rows.Count >= 1 Then
                Dim MemberID As String = objDS.Tables(0).Rows(0)("MemberRECID").ToString
                Dim LoginCode As String = objDS.Tables(0).Rows(0)("LoginCode").ToString

                FormsAuthentication.SetAuthCookie("!234" + "_MGMT", False)


                Response.Redirect("Waiting_Page.aspx?id=" & MemberID & "&code='" & LoginCode & "'", False)



            End If


        Catch ex As Exception
            If blnShow_Dev_Err_Msg = False Then
                Response.Write(ex.Message + " - FAIL to load driver.")
            End If
            If blnShow_Dev_Err_Msg = True Then
                Response.Write(ex.Message + " " + strSQL)
            End If
        Finally
            objDB = Nothing
        End Try


    End Sub


    Protected Sub Timer1_Tick(ByVal sender As Object, e As EventArgs)
        loadPassengerDetail()

    End Sub



End Class
