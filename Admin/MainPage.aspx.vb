﻿Imports System.Data
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports Telerik.Web.UI

Partial Class Admin_MainPage
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            Page.Header.DataBind()
            RadGrid1.MasterTableView.ExpandCollapseColumn.Visible = False
            RadGrid1.ExportSettings.ExportOnlyData = True
            loadProfileInfo()

            RadGrid1.ExportSettings.Pdf.BorderType = GridPdfSettings.GridPdfBorderType.AllBorders

        End If
    End Sub

    Protected Sub RadGrid1_ColumnCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridColumnCreatedEventArgs) Handles RadGrid1.ColumnCreated

        If (e.Column.UniqueName Is "ExpandColumn") Then

            e.Column.Display = False
        End If

    End Sub

    Protected Sub DownloadPDF_Click(sender As Object, e As EventArgs)

        RadGrid1.MasterTableView.GetColumn("ImageBtn").Display = False
        RadGrid1.MasterTableView.GetColumn("No").Display = True
        RadGrid1.MasterTableView.DetailTables(0).GetColumn("TemplateColumn").Visible = False

        RadGrid1.MasterTableView.GetColumn("Select").Display = False

        RadGrid1.ExportSettings.ExportOnlyData = True

        RadGrid1.MasterTableView.ExportToPdf()

    End Sub

    Protected Sub Download_Receipt(sender As Object, e As EventArgs)

        Dim objDB As Database
        Dim strOrderID As String
        Dim objDS As DataSet
        Dim strSql As String
        Dim strName As String = ""
        Dim strRECID As String = Request.QueryString("id")
        Dim strDescription As String = ""
        Dim blnShow_Dev_Err_Msg As Boolean = System.Configuration.ConfigurationManager.AppSettings("Show_Dev_Err_Msg")
        Dim strWHERE As String = ""

        Dim TheArray As String() = order1.Value.Split("|||")

        For Each item As GridDataItem In RadGrid1.SelectedItems
            strOrderID = item.Cells(4).Text

            Try

                strSql = "SELECT tblOrder.RECID, tblOrder.OrderID, tblPassenger.Name, tblPassenger.Email,  tblOrder.Discount, tblOrder.PaymentType, tblOrder.Fare, tblOrder.TotalFare, tblOrder.TotalToll, tblOrder.AddTo, "
                strSql += "tblOrder.AddFrom, tblOrder.MemberRECID, tblOrder.CreateDate, tblOrder.TaxiDriver, tblOrder.TaxiNo, "
                strSql += "tblOrder.EstDistFromTo, tblOrder.PromoCodeRECID, "
                strSql += "CASE WHEN tblOrder.isBusiness = 1 THEN 'Business' Else 'Personal' END AS isBusiness "
                strSql += "From tblOrder "
                strSql += "INNER JOIN tblPassenger ON tblOrder.MemberRECID = tblPassenger.RECID "
                strSql += "WHERE tblOrder.OrderID = '" & CommonLib.SQLStrEncode(strOrderID) & "'"

                objDB = DatabaseFactory.CreateDatabase()
                objDS = objDB.ExecuteDataSet(CommandType.Text, strSql)

                SendEmail.PrintReceipt(objDS.Tables(0).Rows(0)("OrderID").ToString(), objDS.Tables(0).Rows(0)("Name").ToString(), objDS.Tables(0).Rows(0)("Email").ToString(), objDS.Tables(0).Rows(0)("MemberRECID").ToString(), objDS.Tables(0).Rows(0)("TotalFare").ToString(), objDS.Tables(0).Rows(0)("PromoCodeRECID").ToString(), objDS.Tables(0).Rows(0)("RECID").ToString(),
                                       objDS.Tables(0).Rows(0)("PaymentType").ToString(), objDS.Tables(0).Rows(0)("CreateDate").ToString(),
                                       objDS.Tables(0).Rows(0)("TaxiDriver").ToString(), objDS.Tables(0).Rows(0)("TaxiNo").ToString(), objDS.Tables(0).Rows(0)("IsBusiness").ToString(),
                                       objDS.Tables(0).Rows(0)("EstDistFromTo").ToString(), objDS.Tables(0).Rows(0)("AddFrom").ToString(), objDS.Tables(0).Rows(0)("AddTo").ToString(), objDS.Tables(0).Rows(0)("Discount").ToString(), objDS.Tables(0).Rows(0)("TotalToll").ToString(), objDS.Tables(0).Rows(0)("Fare").ToString())
            Catch ex As Exception
                If blnShow_Dev_Err_Msg = False Then
                    Response.Write(ex.Message + " - FAIL to get Category list.")
                End If
                If blnShow_Dev_Err_Msg = True Then
                    Response.Write(ex.Message + " " + strSql)
                End If
            End Try

        Next

    End Sub

    Protected Sub RadGrid1_ItemCommand(ByVal sender As Object, ByVal e As GridCommandEventArgs) Handles RadGrid1.ItemCommand

        If (e.CommandName Is "download") Then

            RadClientExportManager1.PdfSettings.ProxyURL = ResolveUrl("~/Templates/")

        End If

    End Sub

    Protected Sub RadGrid1_ItemCreated(ByVal sender As Object, e As Telerik.Web.UI.GridItemEventArgs) Handles RadGrid1.ItemCreated

        If TypeOf e.Item Is GridPagerItem Then

            Dim pagerItem As GridPagerItem = TryCast(e.Item, GridPagerItem)
            Dim lnkButton As LinkButton = TryCast(pagerItem.FindControl("lnkEmail"), LinkButton)
            Dim lblPage As Label = TryCast(pagerItem.FindControl("lblPage"), Label)
            lnkButton.Text = RadGrid1.MasterTableView.GetSelectedItems.Length.ToString()

        End If

    End Sub

    Protected Sub RadGrid1_DetailTableDataBind(ByVal source As Object, ByVal e As GridDetailTableDataBindEventArgs) Handles RadGrid1.DetailTableDataBind
        Dim parentItem As GridDataItem = CType(e.DetailTableView.ParentItem, GridDataItem)
        Dim objDB As Database
        Dim objDS As DataSet

        Dim blnShow_Dev_Err_Msg As Boolean = System.Configuration.ConfigurationManager.AppSettings("Show_Dev_Err_Msg")
        Dim dataItem As GridDataItem = DirectCast(e.DetailTableView.ParentItem, GridDataItem)
        Dim strSql As String
        Dim RECID As String = dataItem.GetDataKeyValue("RECID").ToString()

        Try

            strSql = "SELECT PromoCodeRECID, RECID, TotalFare, TotalToll, PaymentType,  CreateDate, TaxiDriver, TaxiNo,  EstDistFromTo, AddFrom, AddTo, OrderID, TotalToll,  "
            strSql += "CASE WHEN isBusiness = 1 THEN 'Business' Else 'Personal' END AS isBusiness, "
            strSql += "(SELECT Code FROM tblSetting_PromoCode WHERE RECID = PromoCodeRECID) AS Code, "
            strSql += "(SELECT Rate FROM tblLog_Rate_Driver WHERE OrderRECID = A.RECID) AS Rate, "
            strSql += "(SELECT C.TaxiType From tblDriver AS A INNER JOIN  tblOrder AS B ON B.DriverRECID = A.RECID INNER JOIN tblSetting_TaxiType AS C ON C.RECID = A.TaxiTypeRECID WHERE B.RECID = '" & CommonLib.SQLStrEncode(RECID) & "') AS TaxiType, "
            strSql += "(SELECT TaxiProductType FROM tblSetting_TaxiProductType WHERE RECID = A.TaxiProductTypeRECID) AS TaxiProduct "
            strSql += "FROM tblOrder AS A "
            strSql += "WHERE RECID = '" & CommonLib.SQLStrEncode(RECID) & "' "

            strSql += "Order By CreateDate Desc "

            objDB = DatabaseFactory.CreateDatabase()
            objDS = objDB.ExecuteDataSet(CommandType.Text, strSql)

            RadGrid1.DataSource = objDS.Tables(0)

            Select Case e.DetailTableView.Name

                Case "Detail"
                    If True Then

                        e.DetailTableView.DataSource = objDS.Tables(0)

                        Exit Select
                    End If
            End Select
        Catch ex As Exception
            If blnShow_Dev_Err_Msg = False Then
                Response.Write(ex.Message + " - FAIL to get Category list.")
            End If
            If blnShow_Dev_Err_Msg = True Then
                Response.Write(ex.Message + " " + strSql)
            End If
        End Try

    End Sub

    Private Sub loadProfileInfo()
        Dim objDB As Database
        Dim objDS As DataSet

        Dim blnShow_Dev_Err_Msg As Boolean = System.Configuration.ConfigurationManager.AppSettings("Show_Dev_Err_Msg")
        Dim strSql As String

        Dim strRECID As String = Request.QueryString("id")

        Try

            strSql = "Select Name, Email, PhoneNo, ProfilePicture From tblPassenger "
            strSql += "WHERE RECID = '" & CommonLib.SQLStrEncode(strRECID) & "' "

            objDB = DatabaseFactory.CreateDatabase()
            objDS = objDB.ExecuteDataSet(CommandType.Text, strSql)

            profileName.InnerText = objDS.Tables(0).Rows(0)("Name")
            profileEmail.InnerText = objDS.Tables(0).Rows(0)("Email")
            profileMobileNo.InnerText = objDS.Tables(0).Rows(0)("PhoneNo")
            lblName.Text = objDS.Tables(0).Rows(0)("Name")
            If Not objDS.Tables(0).Rows(0)("ProfilePicture").ToString.Equals("") Then
                imgAvatar.Src = objDS.Tables(0).Rows(0)("ProfilePicture")
            Else

                imgAvatar.Src = "~/Admin/pic/profile_ori.png"

            End If
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub RadGrid1_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles RadGrid1.ItemDataBound
        Dim objDB As Database
        Dim objDS As DataSet

        Dim blnShow_Dev_Err_Msg As Boolean = System.Configuration.ConfigurationManager.AppSettings("Show_Dev_Err_Msg")
        Dim strSql As String
        Select Case e.Item.ItemType

            Case Telerik.Web.UI.GridItemType.Item, Telerik.Web.UI.GridItemType.AlternatingItem

                For Each dataItem As GridDataItem In RadGrid1.MasterTableView.Items

                    Dim objLbl As Label = dataItem.FindControl("lblAutoNo")

                    objLbl.Text = (RadGrid1.MasterTableView.CurrentPageIndex * RadGrid1.MasterTableView.PageSize) + (dataItem.ItemIndex + 1)

                Next

                If e.Item.OwnerTableView.Name Is "Detail" Then

                    Dim ltrrating As Literal = CType(e.Item.FindControl("ltrRating"), Literal)
                    Dim hldRating As HiddenField = CType(e.Item.FindControl("hldRating"), HiddenField)

                    If Len(hldRating.Value) > 0 Then
                        For intCnt As Integer = 0 To hldRating.Value - 1
                            ltrrating.Text += "<img width=""12"" height=""12"" src=""pic/driver_rate.png"" />"
                        Next
                    End If

                End If

        End Select

    End Sub

    Protected Sub RadGrid1_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles RadGrid1.NeedDataSource
        Dim objDB As Database
        Dim objDS As DataSet
        Dim strSql As String
        Dim strName As String = ""
        Dim strRECID As String = Request.QueryString("id")
        Dim strDescription As String = ""
        Dim blnShow_Dev_Err_Msg As Boolean = System.Configuration.ConfigurationManager.AppSettings("Show_Dev_Err_Msg")
        Dim strWHERE As String = ""
        Dim strStartDateFrom As String = ""
        Dim strEndDateTo As String = ""

        If Not rdpFrom.DbSelectedDate Is Nothing Then
            If Len(strWHERE) > 0 Then strWHERE += "AND "
            strWHERE += "DATEDIFF(D,'" & Format(rdpFrom.DbSelectedDate, "dd/MMM/yyyy") & "',CreateDate) >= 0  "

        End If

        If Not rdpTo.DbSelectedDate Is Nothing Then
            If Len(strWHERE) > 0 Then strWHERE += "AND "
            strWHERE += "DATEDIFF(D,CreateDate,'" & Format(rdpTo.DbSelectedDate, "dd/MMM/yyyy") & "') >= 0 "

        End If

        If ddlStatus.SelectedIndex > 0 Then
            If Len(strWHERE) > 0 Then strWHERE += "AND "
            strWHERE += "IsBusiness = '" & CommonLib.SQLStrEncode(ddlStatus.SelectedValue) & "' "
        End If

        Try

            strSql = "SELECT RECID, OrderID, CreateDate, TotalFare, UPPER(Left(PaymentType,1))+LOWER(RIGHT(PaymentType,Len(PaymentType)-1)) AS PaymentType, "
            strSql += "CASE WHEN isBusiness = 1 THEN 'Business' Else 'Personal' END AS isBusiness, "
            strSql += "CASE WHEN AddToName = '' OR AddToName IS NULL then AddTo else AddToName END AS AddToName, "
            strSql += "CASE WHEN AddFromName = '' OR AddFromName IS NULL then AddFrom else AddFromName END AS AddFromName "
            strSql += "From tblOrder "
            strSql += "WHERE MemberRECID = '" & CommonLib.SQLStrEncode(strRECID) & "' AND Status = 'A' "

            If Len(strWHERE) > 0 Then
                strSql += "AND " & strWHERE
            End If

            strSql += "Order By CreateDate Desc "

            objDB = DatabaseFactory.CreateDatabase()
            objDS = objDB.ExecuteDataSet(CommandType.Text, strSql)

            RadGrid1.DataSource = objDS.Tables(0)
        Catch ex As Exception
            If blnShow_Dev_Err_Msg = False Then
                Response.Write(ex.Message + " - FAIL to get Category list.")
            End If
            If blnShow_Dev_Err_Msg = True Then
                Response.Write(ex.Message + " " + strSql)
            End If
        End Try
    End Sub

    Protected Sub lnkSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSearch.Click
        RadGrid1.Rebind()
    End Sub

    Protected Sub lnkSignOut_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSignOut.Click
        Response.Redirect("Index.aspx", False)

    End Sub

End Class