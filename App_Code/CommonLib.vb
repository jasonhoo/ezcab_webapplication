Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports System.IO
Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.Drawing.Imaging
Imports Telerik.Web.UI
Imports System.Web
Imports System.Web.Security
Imports System.Text.RegularExpressions
Imports System
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Collections

Public Class CommonLib
    Public Shared COOKIE_USERLOGIN_MGMT As String = "UserLoginMgmt"
    Public Shared COOKIE_LOGINID As String = "LoginID"
    Public Shared COOKIE_USERID As String = "UserID"
    Public Shared COOKIE_USERTYPE As String = "UserType"
    Public Shared COOKIE_PARTNER As String = "Partner"
    Public Shared COOKIE_ALLOWVIEWALL As String = "AllowViewAll"
    Public Shared COOKIE_ALLOWVIEWCOMPANY As String = "AllowViewCompany"
    Public Shared COOKIE_COMPANYNAME As String = "CompanyName"
    Public Shared COOKIE_DATETIME As Date = Date.Now

    Public Shared Function CheckUserRight(ByVal strAdminRECID As String, ByVal strMenuRECID As String) As DataTable
        Dim objDB As Database
        Dim strSQL As String
        Dim objDS As DataSet
        Dim objDTAccessRight As New DataTable

        If HttpContext.Current.Request.IsAuthenticated = False Or Len(HttpContext.Current.Session("UserRECID")) = 0 Then
            '//FormsAuthentication.RedirectToLoginPage()
            HttpContext.Current.Server.Transfer(FormsAuthentication.LoginUrl())
        End If

        If Len(strMenuRECID) = 0 Then
            '//Redirect to welcome page
            HttpContext.Current.Server.Transfer("Welcome.aspx")
        End If

        strSQL = "SELECT IsView, IsAdd, IsModify, IsDelete "
        strSQL += "FROM tblAdmin_AccessRight "
        strSQL += "WHERE AdminRECID = '" & strAdminRECID & "' AND MenuRECID = '" & strMenuRECID & "' "

        objDB = DatabaseFactory.CreateDatabase()
        objDS = objDB.ExecuteDataSet(CommandType.Text, strSQL)
        objDTAccessRight = objDS.Tables(0)
        objDB = Nothing

        Return objDTAccessRight
    End Function

    Public Shared Function RenameFile(ByVal strFileName As String) As String
        Dim strNewFileName As String = ""

        strNewFileName = Replace(strFileName, "'", "")
        strNewFileName = Replace(strNewFileName, " ", "")
        strNewFileName = Replace(strNewFileName, "/", "")
        strNewFileName = Replace(strNewFileName, "\", "")
        strNewFileName = Replace(strNewFileName, ":", "")
        strNewFileName = Replace(strNewFileName, "*", "")
        strNewFileName = Replace(strNewFileName, "?", "")
        strNewFileName = Replace(strNewFileName, "<", "")
        strNewFileName = Replace(strNewFileName, ">", "")
        strNewFileName = Replace(strNewFileName, "|", "")
        strNewFileName = Replace(strNewFileName, "&", "")

        Return strNewFileName
    End Function

    Public Shared Function RenameFile(ByVal strOriFileName As String, ByVal strSeqNo As String) As String
        Dim strNewFileName As String = ""

        strNewFileName = Replace(strOriFileName, "'", "")
        strNewFileName = Replace(strNewFileName, " ", "")
        strNewFileName = Replace(strNewFileName, "/", "")
        strNewFileName = Replace(strNewFileName, "\", "")
        strNewFileName = Replace(strNewFileName, ":", "")
        strNewFileName = Replace(strNewFileName, "*", "")
        strNewFileName = Replace(strNewFileName, "?", "")
        strNewFileName = Replace(strNewFileName, "<", "")
        strNewFileName = Replace(strNewFileName, ">", "")
        strNewFileName = Replace(strNewFileName, "|", "")
        strNewFileName = Replace(strNewFileName, "&", "")
        strNewFileName = strNewFileName + "_" + strSeqNo

        Return strNewFileName
    End Function

    Public Shared Function AddressAbbrev(ByVal strAddress As String) As String
        Dim strNewAddress As String = strAddress
        Dim objDB As Database
        Dim objDS As DataSet
        Dim strSQL As String = ""

        strSQL = "SELECT Abbrev, FullName FROM tblSetting_Abbrev WHERE Status = 1 "
        objDB = DatabaseFactory.CreateDatabase
        objDS = objDB.ExecuteDataSet(CommandType.Text, strSQL)
        If objDS.Tables(0).Rows.Count > 0 Then
            For intCnt As Integer = 0 To objDS.Tables(0).Rows.Count - 1
                strNewAddress = Replace(strNewAddress, objDS.Tables(0).Rows(intCnt)("Abbrev").ToString, objDS.Tables(0).Rows(intCnt)("FullName").ToString, , , CompareMethod.Text)
            Next
        End If

        objDB = Nothing
        Return strNewAddress
    End Function

    Public Shared Function RemoveHtmlTag(ByVal strBody As String) As String
        Dim strResult As String
        Dim strError As String = ""

        Try
            strResult = Regex.Replace(strBody, "<(.|\n)*?>", "")
        Catch e As Exception
            strError = e.Message
            strResult = ""
        End Try

        Return strResult

    End Function

    Public Shared Function GetMD5String(ByVal strCode As String) As String
        Dim objEncoder As New System.Text.UTF8Encoding
        Dim objEncryp As New System.Security.Cryptography.MD5CryptoServiceProvider()
        Dim bytHashedByte As Byte()
        Dim objSTRBuilder As New System.Text.StringBuilder

        '//Get Hashed Key in bytes
        bytHashedByte = objEncryp.ComputeHash(objEncoder.GetBytes(strCode))

        '//Extract Hashed key in bytes to string
        For Each bytHashed As Byte In bytHashedByte
            objSTRBuilder.Append(bytHashed.ToString("x2").ToLower())
        Next

        Return objSTRBuilder.ToString()
    End Function

    '//Encode single Quo ' to be ''
    Public Shared Function SQLStrEncode(ByVal strText As String) As String
        Return Replace(NullEncode(strText), "'", "''")
    End Function

    '// Workaround if recordset return NULL value
    Public Shared Function NullEncode(ByVal strValue As String) As String
        Return "" & strValue
    End Function

    Public Shared Function SanitizeURLString(ByVal RawURLParameter As String) As String

        Dim Results As String

        Results = RawURLParameter

        'Results = Results.Replace("<", "%3C")
        'Results = Results.Replace(">", "%3E")
        'Results = Results.Replace("#", "%23")
        'Results = Results.Replace("%", "%25")
        'Results = Results.Replace("{", "%7B")
        'Results = Results.Replace("}", "%7D")
        'Results = Results.Replace("|", "%7C")
        'Results = Results.Replace("\", "%5C")
        'Results = Results.Replace("^", "%5E")
        'Results = Results.Replace("~", "%7E")
        'Results = Results.Replace("[", "%5B")
        'Results = Results.Replace("]", "%5D")
        'Results = Results.Replace("`", "%60")
        'Results = Results.Replace(";", "%3B")
        'Results = Results.Replace("/", "%2F")
        'Results = Results.Replace("?", "%3F")
        'Results = Results.Replace(":", "%3A")
        'Results = Results.Replace("@", "%40")
        'Results = Results.Replace("=", "%3D")
        'Results = Results.Replace("&", "%26")
        'Results = Results.Replace("$", "%24")
        Results = Results.Replace("'", "%27")

        Return Results

    End Function

    Public Shared Function GetBaseURL() As String
        Dim strBaseURL As String

        strBaseURL = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + HttpContext.Current.Request.ApplicationPath.TrimEnd("/") + "/"
        Return strBaseURL
    End Function

    Public Shared Sub ShowJSMsg(ByVal objPage As Page, ByVal strMessage As String, Optional ByVal strRedirect As String = "")
        Dim strScript As String

        strScript = "<script language = ""Javascript"" type=""text/javascript"" > "
        strScript = strScript & "alert('" & strMessage & "'); "
        If strRedirect.Length > 0 Then
            strScript = strScript & "document.location.href = '" & strRedirect & "'; "
        End If
        strScript = strScript & "</script> "
        objPage.ClientScript.RegisterStartupScript(objPage.GetType(), "alertRedirect", strScript)
    End Sub

    Public Shared Sub ShowAjaxMsg(ByVal objPage As Page, ByVal strMessage As String, Optional ByVal strRedirect As String = "")
        Dim objAjaxMgr As RadAjaxManager = RadAjaxManager.GetCurrent(objPage)
        Dim strScript As String = ""
        strScript = "alert('" & strMessage & "'); "
        If Len(strRedirect) > 0 Then strScript += "document.location.href = '" & strRedirect & "'; "
        objAjaxMgr.ResponseScripts.Add(strScript)
    End Sub

    Public Shared Sub TrigerJSFunction(ByVal objPage As Page, ByVal strFunction As String)
        Dim strScript As String = "<script language = ""Javascript"" type=""text/javascript"" > "
        strScript = strScript & strFunction
        strScript = strScript & "</script> "
        objPage.ClientScript.RegisterStartupScript(objPage.GetType(), "JsFunction", strScript)
    End Sub

    Public Shared Sub TrigerAjaxFunction(ByVal objPage As Page, ByVal strFunction As String)
        Dim objAjaxMgr As RadAjaxManager = RadAjaxManager.GetCurrent(objPage)
        objAjaxMgr.ResponseScripts.Add(strFunction)
    End Sub

    Public Shared Sub GetImgFileType(ByVal lblFileType As Label, ByVal RadAsyncUpload1 As Telerik.Web.UI.RadAsyncUpload)
        Dim strSQL As String
        Dim objDB As Database
        objDB = DatabaseFactory.CreateDatabase()
        strSQL = "SELECT ImgFileType FROM tblSetting_Master "
        Dim objDR As IDataReader = objDB.ExecuteReader(CommandType.Text, strSQL)
        objDR.Read()
        Dim strFileType As String = objDR.GetValue(objDR.GetOrdinal("ImgFileType")).ToString.Trim()
        objDR.Close()
        lblFileType.Text = strFileType
        Dim strAllowedExtensions As String() = strFileType.Split(New Char() {","c})
        RadAsyncUpload1.AllowedFileExtensions = strAllowedExtensions
    End Sub

    Public Shared Sub GetImgFileSize(ByVal lblFileSize As Label, ByVal RadAsyncUpload1 As Telerik.Web.UI.RadAsyncUpload)
        Dim strSQL As String
        Dim objDB As Database
        Dim dbFileSize As Double
        objDB = DatabaseFactory.CreateDatabase()
        strSQL = "SELECT ImgFileSize FROM tblSetting_Master "
        Dim objDR As IDataReader = objDB.ExecuteReader(CommandType.Text, strSQL)
        objDR.Read()
        RadAsyncUpload1.MaxFileSize = objDR("ImgFileSize").ToString
        dbFileSize = Convert.ToDouble(objDR("ImgFileSize"))
        objDR.Close()

        If dbFileSize >= 1024 Then
            dbFileSize = dbFileSize / 1024

            If dbFileSize < 1024 Then
                lblFileSize.Text = dbFileSize.ToString("F") & " kb"
            End If
        End If

        If dbFileSize >= 1024 Then
            dbFileSize = dbFileSize / 1024

            If dbFileSize < 1024 Then
                lblFileSize.Text = dbFileSize.ToString("F") & " mb"
            End If
        End If

        If dbFileSize >= 1024 Then
            dbFileSize = dbFileSize / 1024

            If dbFileSize < 1024 Then
                lblFileSize.Text = dbFileSize.ToString("F") & " gb"
            End If
        End If
    End Sub

    Public Shared Sub GetAttachFileType(ByVal lblFileType As Label, ByVal RadAsyncUpload1 As Telerik.Web.UI.RadAsyncUpload)
        Dim strSQL As String
        Dim objDB As Database
        objDB = DatabaseFactory.CreateDatabase()
        strSQL = "SELECT AttachFileType FROM tblSetting_Master "
        Dim objDR As IDataReader = objDB.ExecuteReader(CommandType.Text, strSQL)
        objDR.Read()
        Dim strFileType As String = objDR.GetValue(objDR.GetOrdinal("AttachFileType")).ToString.Trim()
        objDR.Close()
        lblFileType.Text = strFileType
        Dim strAllowedExtensions As String() = strFileType.Split(New Char() {","c})
        RadAsyncUpload1.AllowedFileExtensions = strAllowedExtensions
    End Sub

    Public Shared Sub GetAttachFileSize(ByVal lblFileSize As Label, ByVal RadAsyncUpload1 As Telerik.Web.UI.RadAsyncUpload)
        Dim strSQL As String
        Dim objDB As Database
        Dim dbFileSize As Double
        objDB = DatabaseFactory.CreateDatabase()
        strSQL = "SELECT AttachFileSize FROM tblSetting_Master "
        Dim objDR As IDataReader = objDB.ExecuteReader(CommandType.Text, strSQL)
        objDR.Read()
        RadAsyncUpload1.MaxFileSize = objDR("AttachFileSize").ToString
        dbFileSize = Convert.ToDouble(objDR("AttachFileSize"))
        objDR.Close()

        If dbFileSize >= 1024 Then
            dbFileSize = dbFileSize / 1024

            If dbFileSize < 1024 Then
                lblFileSize.Text = dbFileSize.ToString("F") & " kb"
            End If
        End If

        If dbFileSize >= 1024 Then
            dbFileSize = dbFileSize / 1024

            If dbFileSize < 1024 Then
                lblFileSize.Text = dbFileSize.ToString("F") & " mb"
            End If
        End If

        If dbFileSize >= 1024 Then
            dbFileSize = dbFileSize / 1024

            If dbFileSize < 1024 Then
                lblFileSize.Text = dbFileSize.ToString("F") & " gb"
            End If
        End If
    End Sub

    Public Shared Sub DeleteAllSubFolders(ByVal StartPath As String)
        Dim myfolder As DirectoryInfo = New DirectoryInfo(StartPath)
        Dim mySubfolders() As DirectoryInfo = myfolder.GetDirectories()
        Dim strFiles() As FileInfo = myfolder.GetFiles()
        For Each myItem As DirectoryInfo In mySubfolders
            DeleteAllSubFolders(myItem.FullName)
        Next
        For Each myItem As FileInfo In strFiles
            myItem.Attributes = FileAttributes.Normal
            myItem.Delete()
        Next
        myfolder.Delete()
    End Sub

    Public Shared Sub DeleteFolder(ByVal strPath As String)
        Dim myfolder As DirectoryInfo = New DirectoryInfo(strPath)
        Dim strFiles() As FileInfo = myfolder.GetFiles()
        For Each myItem As FileInfo In strFiles
            myItem.Attributes = FileAttributes.Normal
            myItem.Delete()
        Next
        myfolder.Delete()
    End Sub

    Public Shared Sub DeleteFilesInFolder(ByVal strPath As String)
        Dim myfolder As DirectoryInfo = New DirectoryInfo(strPath)
        Dim strFiles() As FileInfo = myfolder.GetFiles()
        For Each myItem As FileInfo In strFiles
            myItem.Attributes = FileAttributes.Normal
            myItem.Delete()
        Next
    End Sub

    Public Shared Sub GenerateWatermark(ByVal strImgPath As String, ByVal strWaterMarkPath As String, ByVal strDirPath As String, ByVal strImgName As String, ByVal strWatermarkText As String, ByVal strMode As String)
        'strMode 1 = add text only
        'strMode 2 = add image only
        'strMode 3 = add text and image

        'define a string of text to use as the Copyright message
        Dim Copyright As String = strWatermarkText

        'create a image object containing the photograph to watermark
        Dim imgPhoto As System.Drawing.Image = System.Drawing.Image.FromFile(strImgPath)
        Dim phWidth As Integer = imgPhoto.Width
        Dim phHeight As Integer = imgPhoto.Height

        'create a Bitmap the Size of the original photograph
        Dim bmPhoto As New Bitmap(phWidth, phHeight, PixelFormat.Format24bppRgb)

        bmPhoto.SetResolution(imgPhoto.HorizontalResolution, imgPhoto.VerticalResolution)

        'load the Bitmap into a Graphics object 
        Dim grPhoto As Graphics = Graphics.FromImage(bmPhoto)

        '------------------------------------------------------------
        'Step #1 - Insert Copyright message
        '------------------------------------------------------------
        'Set the rendering quality for this Graphics object
        grPhoto.SmoothingMode = SmoothingMode.AntiAlias

        'Draws the photo Image object at original size to the graphics object.
        ' Photo Image object
        ' Rectangle structure
        ' x-coordinate of the portion of the source image to draw. 
        ' y-coordinate of the portion of the source image to draw. 
        ' Width of the portion of the source image to draw. 
        ' Height of the portion of the source image to draw. 
        grPhoto.DrawImage(imgPhoto, New Rectangle(0, 0, phWidth, phHeight), 0, 0, phWidth, phHeight, GraphicsUnit.Pixel)

        If strMode = "1" Or strMode = "3" Then
            ' Units of measure 
            '-------------------------------------------------------
            'to maximize the size of the Copyright message we will 
            'test multiple Font sizes to determine the largest posible 
            'font we can use for the width of the Photograph
            'define an array of point sizes you would like to consider as possiblities
            '-------------------------------------------------------
            Dim sizes As Integer() = New Integer() {16, 14, 12, 10, 8, 6, _
             4}

            Dim crFont As Font = Nothing
            Dim crSize As New SizeF()

            'Loop through the defined sizes checking the length of the Copyright string
            'If its length in pixles is less then the image width choose this Font size.
            For i As Integer = 0 To 6
                'set a Font object to Arial (i)pt, Bold
                crFont = New Font("arial", sizes(i), FontStyle.Bold)
                'Measure the Copyright string in this Font
                crSize = grPhoto.MeasureString(Copyright, crFont)

                If CUShort(crSize.Width) < CUShort(phWidth) Then
                    Exit For
                End If
            Next

            'Since all photographs will have varying heights, determine a 
            'position 5% from the bottom of the image
            Dim yPixlesFromBottom As Integer = CInt(Math.Truncate(phHeight * 0.15))

            'Now that we have a point size use the Copyrights string height 
            'to determine a y-coordinate to draw the string of the photograph
            Dim yPosFromBottom As Single = ((phHeight - yPixlesFromBottom) - (crSize.Height / 2))

            'Determine its x-coordinate by calculating the center of the width of the image
            Dim xCenterOfImg As Single = (phWidth \ 2)

            'Define the text layout by setting the text alignment to centered
            Dim StrFormat As New StringFormat()
            StrFormat.Alignment = StringAlignment.Center

            'define a Brush which is semi trasparent black (Alpha set to 153)
            Dim semiTransBrush2 As New SolidBrush(Color.FromArgb(153, 0, 0, 0))

            'Draw the Copyright string
            'string of text
            'font
            'Brush
            'Position
            grPhoto.DrawString(Copyright, crFont, semiTransBrush2, New PointF(xCenterOfImg + 1, yPosFromBottom + 1), StrFormat)

            'define a Brush which is semi trasparent white (Alpha set to 153)
            Dim semiTransBrush As New SolidBrush(Color.FromArgb(153, 255, 255, 255))

            'Draw the Copyright string a second time to create a shadow effect
            'Make sure to move this text 1 pixel to the right and down 1 pixel
            'string of text
            'font
            'Brush
            'Position
            grPhoto.DrawString(Copyright, crFont, semiTransBrush, New PointF(xCenterOfImg, yPosFromBottom), StrFormat)
            'Text alignment

            'ImageAttributes Object
            'Replace the original photgraphs bitmap with the new Bitmap
            If strMode = "1" Then
                imgPhoto = bmPhoto
            End If

        End If

        '------------------------------------------------------------
        'Step #2 - Insert Watermark image
        '------------------------------------------------------------
        If strMode = "2" Or strMode = "3" Then
            'create a image object containing the watermark
            Dim imgWatermark As System.Drawing.Image = New Bitmap(strWaterMarkPath)
            Dim wmWidth As Integer = imgWatermark.Width
            Dim wmHeight As Integer = imgWatermark.Height

            'Create a Bitmap based on the previously modified photograph Bitmap
            Dim bmWatermark As New Bitmap(bmPhoto)
            bmWatermark.SetResolution(imgPhoto.HorizontalResolution, imgPhoto.VerticalResolution)
            'Load this Bitmap into a new Graphic Object
            Dim grWatermark As Graphics = Graphics.FromImage(bmWatermark)

            'To achieve a transulcent watermark we will apply (2) color 
            'manipulations by defineing a ImageAttributes object and 
            'seting (2) of its properties.
            Dim imageAttributes As New ImageAttributes()

            'The first step in manipulating the watermark image is to replace 
            'the background color with one that is trasparent (Alpha=0, R=0, G=0, B=0)
            'to do this we will use a Colormap and use this to define a RemapTable
            Dim colorMap As New ColorMap()

            'My watermark was defined with a background of 100% Green this will
            'be the color we search for and replace with transparency
            colorMap.OldColor = Color.FromArgb(255, 0, 255, 0)
            colorMap.NewColor = Color.FromArgb(0, 0, 0, 0)

            Dim remapTable As ColorMap() = {colorMap}

            imageAttributes.SetRemapTable(remapTable, ColorAdjustType.Bitmap)

            'The second color manipulation is used to change the opacity of the 
            'watermark.  This is done by applying a 5x5 matrix that contains the 
            'coordinates for the RGBA space.  By setting the 3rd row and 3rd column 
            'to 0.3f we achive a level of opacity
            Dim colorMatrixElements As Single()() = {New Single() {1.0F, 0.0F, 0.0F, 0.0F, 0.0F}, New Single() {0.0F, 1.0F, 0.0F, 0.0F, 0.0F}, New Single() {0.0F, 0.0F, 1.0F, 0.0F, 0.0F}, New Single() {0.0F, 0.0F, 0.0F, 0.3F, 0.0F}, New Single() {0.0F, 0.0F, 0.0F, 0.0F, 1.0F}}
            Dim wmColorMatrix As New ColorMatrix(colorMatrixElements)

            imageAttributes.SetColorMatrix(wmColorMatrix, ColorMatrixFlag.[Default], ColorAdjustType.Bitmap)

            'For this example we will place the watermark in the upper right
            'hand corner of the photograph. offset down 10 pixels and to the 
            'left 10 pixles

            Dim xPosOfWm As Integer = ((phWidth - wmWidth) - 10)
            Dim yPosOfWm As Integer = 10

            'Set the detination Position
            ' x-coordinate of the portion of the source image to draw. 
            ' y-coordinate of the portion of the source image to draw. 
            ' Watermark Width
            ' Watermark Height
            ' Unit of measurment
            grWatermark.DrawImage(imgWatermark, New Rectangle(xPosOfWm, yPosOfWm, wmWidth, wmHeight), 0, 0, wmWidth, wmHeight, GraphicsUnit.Pixel, imageAttributes)

            'ImageAttributes Object
            'Replace the original photgraphs bitmap with the new Bitmap
            imgPhoto = bmWatermark
            grWatermark.Dispose()
            imgWatermark.Dispose()
        End If

        grPhoto.Dispose()

        'save new image to file system.
        'imgPhoto.Save(strDirPath & "\temp_" & strImgName, ImageFormat.Jpeg)
        'imgPhoto.Dispose()
    End Sub

    Public Shared Function GetFolderSize(ByVal DirPath As String, Optional ByVal IncludeSubFolders As Boolean = True) As Long
        Dim lngFolderSize As Long
        Dim objFileInfo As FileInfo
        Dim objFolder As DirectoryInfo
        Dim objSubFolder As DirectoryInfo
        Try
            objFolder = New DirectoryInfo(DirPath)
            For Each objFileInfo In objFolder.GetFiles()
                lngFolderSize += objFileInfo.Length
            Next
            If IncludeSubFolders Then
                For Each objSubFolder In objFolder.GetDirectories()
                    lngFolderSize += GetFolderSize(objSubFolder.FullName)
                Next
            End If
        Catch Ex As Exception
            MsgBox(Ex.Message)
        End Try
        Return lngFolderSize
    End Function

    Public Shared Sub GenYear(ByRef objControl As Web.UI.WebControls.DropDownList, Optional ByVal intStartYear As Integer = 0, Optional ByVal intEndYear As Integer = 0, Optional ByVal blnReverseYear As Boolean = False)
        Dim objArray As New ArrayList
        Dim intCount As Integer
        Dim intMax As Integer
        Dim intStart As Integer
        Dim objItem As New ListItem

        intStart = 1900
        If intEndYear = 0 Then
            intMax = DateTime.Today().Year
        End If
        intMax = intEndYear

        If intStartYear > 0 Then
            intStart = intStartYear
        End If

        If blnReverseYear = False Then
            For intCount = intStart To intMax
                objArray.Add(intCount)
            Next
        End If

        If blnReverseYear = True Then
            For intCount = intStart To intMax Step -1
                objArray.Add(intCount)
            Next
        End If

        objControl.DataSource = objArray
        objControl.DataBind()
    End Sub

    Public Shared Sub GenYearComboBox(ByRef objControl As Telerik.Web.UI.RadComboBox, Optional ByVal intStartYear As Integer = 0, Optional ByVal intEndYear As Integer = 0, Optional ByVal blnReverseYear As Boolean = False)
        Dim objArray As New ArrayList
        Dim intCount As Integer
        Dim intMax As Integer
        Dim intStart As Integer
        Dim objItem As New ListItem

        intStart = 1900
        If intEndYear = 0 Then
            intMax = DateTime.Today().Year
        End If
        intMax = intEndYear

        If intStartYear > 0 Then
            intStart = intStartYear
        End If

        If blnReverseYear = False Then
            For intCount = intStart To intMax
                objArray.Add(intCount)
            Next
        End If

        If blnReverseYear = True Then
            For intCount = intStart To intMax Step -1
                objArray.Add(intCount)
            Next
        End If

        objControl.DataSource = objArray
        objControl.DataBind()
    End Sub

    Public Shared Function IsSessionExpired() As Boolean
        Dim blnReturn As Boolean = False

        ' //It appears from testing that the Request and Response both share the 
        '// same cookie collection.  If I set a cookie myself in the Reponse, it is 
        '// also immediately visible to the Request collection.  This just means that 
        '// since the ASP.Net_SessionID is set in the Session HTTPModule (which 
        '// has already run), that we can't use our own code to see if the cookie was 
        '// actually sent by the agent with the request using the collection. Check if 
        '// the given page supports session or not (this tested as reliable indicator 
        '// if EnableSessionState is true), should not care about a page that does 
        '// not need session
        If HttpContext.Current.Session Is Nothing = False Then

            ' //Tested and the IsNewSession is more advanced then simply checking if 
            '// a cookie is present, it does take into account a session timeout, because 
            '// I tested a timeout and it did show as a new session
            If HttpContext.Current.Session.IsNewSession = True Then

                '// If it says it is a new session, but an existing cookie exists, then it must 
                '// have timed out (can't use the cookie collection because even on first 
                '// request it already contains the cookie (request and response
                '// seem to share the collection)

                Dim strHeader As String = HttpContext.Current.Request.Headers("Cookie")

                If Len(strHeader) > 0 And InStr(strHeader, "ASP.NET_SessionId", CompareMethod.Text) >= 0 Then
                    '//TO AVOID session cookie still available when user goto other site and come back to the same page, and page trigger the redirect of session timeout.
                    HttpContext.Current.Request.Cookies("ASP.NET_SessionId").Expires = DateTime.Now.AddDays(-100)

                    blnReturn = True
                End If

            End If   '//IsNewSession

        End If '//Context

        Return blnReturn
    End Function

    Public Shared Sub CopyFileToFolder(ByVal strSourceDir As String, ByVal strTargetDir As String, ByVal strFileName As String)
        If Directory.Exists(strTargetDir) = False Then
            Directory.CreateDirectory(strTargetDir)
        End If
        If File.Exists(strSourceDir + "/" + strFileName) = True Then
            If File.Exists(strTargetDir + "/" + strFileName) = False Then
                File.Copy(strSourceDir + "/" + strFileName, strTargetDir + "/" + strFileName)
            End If
            File.Delete(strSourceDir + "/" + strFileName)
        End If
    End Sub

    Public Shared Function GetFileContents(ByVal FullPath As String) As String
        Dim strContents As String
        Dim objReader As StreamReader

        objReader = New StreamReader(FullPath)
        strContents = objReader.ReadToEnd()
        objReader.Close()
        Return strContents

    End Function

    Public Shared Function SaveTextToFile(ByVal strData As String, ByVal FullPath As String) As Boolean
        Dim bAns As Boolean = False
        Dim objReader As StreamWriter
        objReader = New StreamWriter(FullPath)
        objReader.Write(strData)
        objReader.Close()
        bAns = True

        Return bAns
    End Function

    Public Shared Function GetServerDateTime() As Date
        Dim dtServerDateTime As Date
        dtServerDateTime = GetTimeByTimeZone("Singapore Standard Time")
        Return dtServerDateTime
    End Function

    Public Shared Function GetTimeByTimeZone(ByVal strTimezone As String) As Date
        Dim dtTime As Date
        Dim cetZone As TimeZoneInfo
        cetZone = TimeZoneInfo.FindSystemTimeZoneById(strTimezone)
        dtTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, cetZone)
        Return dtTime
    End Function

End Class
