﻿Imports Microsoft.VisualBasic
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports System.Data
Imports System.IO
Imports System

Public Class SendEmail

    Public Shared Function PrintReceipt(ByVal strOrderID As String, ByVal strMemberName As String, ByVal strMemberEmail As String, ByVal strMemberRECID As String, ByVal strTotalFare As String, ByVal strPromoCode As String, ByVal strRECID As String,
                                        ByVal strPaymentType As String, ByVal strCreateDate As String, ByVal strTaxiDriver As String, ByVal strTaxiNo As String, ByVal strIsBusiness As String,
                                        ByVal strEstDistFromTo As String, ByVal strAddFrom As String, ByVal strAddTo As String, ByVal strDiscount As String, ByVal strTotalToll As String, ByVal strFare As String) As Boolean

        Dim blnRet As Boolean = False
        Dim objMail As New aspNetEmail.EmailMessage
        Dim strEmailPath As String = ConfigurationManager.AppSettings("LocalSMTPPickup")
        Dim strEmailFrom As String = ""
        Dim strEmailFromName As String = ""
        Dim strEmailTo As String = ""
        Dim strEmailToName As String = ""

        Dim strEmailAccSubject As String = ""
        Dim strEmailCC As String = ""
        Dim strPath As String = System.Web.HttpRuntime.AppDomainAppPath

        Dim strTemplatePath As String = strPath + ConfigurationManager.AppSettings("TemplatePath") + "/"
        Dim blnLocalSMTP As Boolean = System.Configuration.ConfigurationManager.AppSettings("UseLocalSMTP")
        Dim blnRegUseSMTP As Boolean = System.Configuration.ConfigurationManager.AppSettings("RegUseSMTP")
        Dim strAuthMethod As String = System.Configuration.ConfigurationManager.AppSettings("SMTPAuthMethod")
        Dim blnSMTPAuthReq As Boolean = System.Configuration.ConfigurationManager.AppSettings("SMTPAuthReq")
        Dim strSQL As String
        Dim objDS As DataSet
        Dim objDB As Database
        Dim strSMTP As String = ""
        Dim StrSMTPPW As String = ""
        Dim strSMTPServer As String = ""

        strSQL = "SELECT ToName, ToEmail, FromName, FromEmail, SMTPLogin, SMTPPassword, CCEmail, Subject, Template, SMTPServer From tblSetting_Email WHERE Prefix ='Receipt2' AND AppSrc = 'EzCab' "

        objDB = DatabaseFactory.CreateDatabase
        objDS = objDB.ExecuteDataSet(CommandType.Text, strSQL)

        If objDS.Tables(0).Rows.Count > 0 Then
            strEmailFromName = objDS.Tables(0).Rows(0)("FromName").ToString
            strEmailFrom = objDS.Tables(0).Rows(0)("FromEmail").ToString
            strEmailAccSubject = objDS.Tables(0).Rows(0)("Subject").ToString
            strTemplatePath += objDS.Tables(0).Rows(0)("Template").ToString
            strSMTP = objDS.Tables(0).Rows(0)("SMTPLogin").ToString
            StrSMTPPW = objDS.Tables(0).Rows(0)("SMTPPassword").ToString
            strSMTPServer = objDS.Tables(0).Rows(0)("SMTPServer").ToString

        End If

        Try
            objMail.ValidateAddress = False
            objMail.AddTo(strMemberEmail, strMemberName)
            objMail.FromAddress = strEmailFrom
            objMail.FromName = strEmailFromName
            objMail.Subject = strEmailAccSubject
            objMail.BodyFormat = aspNetEmail.MailFormat.Html

            objMail.GetBodyFromFile(strTemplatePath)
            objMail.Body = Replace(objMail.Body, "##OrderID##", strOrderID, 1, , CompareMethod.Text)
            objMail.Body = Replace(objMail.Body, "##AddTo##", strAddTo, 1, , CompareMethod.Text)
            objMail.Body = Replace(objMail.Body, "##AddFrom##", strAddFrom, 1, , CompareMethod.Text)
            objMail.Body = Replace(objMail.Body, "##Date##", strCreateDate, 1, , CompareMethod.Text)
            objMail.Body = Replace(objMail.Body, "##EstDistFromTo##", strEstDistFromTo, 1, , CompareMethod.Text)
            objMail.Body = Replace(objMail.Body, "##TotalPayment##", strTotalFare, 1, , CompareMethod.Text)
            objMail.Body = Replace(objMail.Body, "##TaxiNo##", strTaxiNo, 1, , CompareMethod.Text)
            objMail.Body = Replace(objMail.Body, "##Name##", strTaxiDriver, 1, , CompareMethod.Text)
            objMail.Body = Replace(objMail.Body, "##PaymentType##", strPaymentType, 1, , CompareMethod.Text)
            objMail.Body = Replace(objMail.Body, "##BookingDate##", strCreateDate, 1, , CompareMethod.Text)
            objMail.Body = Replace(objMail.Body, "##PromoCode##", strPromoCode, 1, , CompareMethod.Text)
            objMail.Body = Replace(objMail.Body, "##TotalFare##", strTotalFare, 1, , CompareMethod.Text)
            objMail.Body = Replace(objMail.Body, "##Toll##", strTotalToll, 1, , CompareMethod.Text)
            objMail.Body = Replace(objMail.Body, "##Discount##", strDiscount, 1, , CompareMethod.Text)
            objMail.Body = Replace(objMail.Body, "##Fare##", strFare, 1, , CompareMethod.Text)
            objMail.Body = Replace(objMail.Body, "##PaymentMethod##", strPaymentType, 1, , CompareMethod.Text)

            If blnLocalSMTP = False Or blnRegUseSMTP = True Then
                objMail.Server = strSMTPServer
                'If strAppSrc.Equals("PublicCab") Then
                '    objMail.Server = "mail.publiccab.com"
                'Else
                '    objMail.Server = System.Configuration.ConfigurationManager.AppSettings("SMTPServer")
                'End If

                If blnSMTPAuthReq = True Then
                    objMail.Username = strSMTP
                    objMail.Password = StrSMTPPW

                    Select Case strAuthMethod.ToUpper()
                        Case "LOGIN"
                            objMail.SmtpAuthentication = aspNetEmail.SmtpAuthentication.AuthLogin
                        Case "TEXT"
                            objMail.SmtpAuthentication = aspNetEmail.SmtpAuthentication.AuthPlain
                        Case "MD5"
                            objMail.SmtpAuthentication = aspNetEmail.SmtpAuthentication.CramMD5
                    End Select
                End If
                objMail.IgnoreRecipientErrors = True
                objMail.Logging = True
                Try
                    objMail.Send()
                Catch ex As Exception
                    Dim strFileName As String = "Receipt_" + Format(Date.Now(), "yyyyMMdd") + ".log"
                    Dim strErr As String = ""
                    strErr = Format(Date.Now(), "yyyy-MM-dd hh:mm:ss tt") + vbTab
                    strErr += objMail.GetLog() + " " + strSQL + vbCrLf

                    strFileName = System.Web.HttpRuntime.AppDomainAppPath + "/Log/" + strFileName
                    My.Computer.FileSystem.WriteAllText(strFileName, strErr, True)

                    blnRet = False
                    Return blnRet
                End Try
            End If

            If blnLocalSMTP = True And blnRegUseSMTP = False Then
                objMail.SendToMSPickup(strEmailPath)
            End If

            blnRet = True

        Catch ex As Exception
            blnRet = False
            Dim strFileName As String = "Receipt_" + Format(Date.Now(), "yyyyMMdd") + ".log"
            Dim strErr As String = ""
            strErr = Format(Date.Now(), "yyyy-MM-dd hh:mm:ss tt") + vbTab
            strErr += ex.Message + " " + strSQL + vbCrLf

            strFileName = System.Web.HttpRuntime.AppDomainAppPath + "/Log/" + strFileName
            My.Computer.FileSystem.WriteAllText(strFileName, strErr, True)
        Finally
            objMail = Nothing
        End Try
        Return blnRet
    End Function
    Public Shared Function PrintReceipt2(ByVal strMemberName As String, ByVal strMemberEmail As String, ByVal strFilePath As String, ByVal strAppSrc As String,
                                         ByVal strOrderID As String, ByVal strAddTo As String, ByVal strAddFrom As String, ByVal strBookingDate As String,
                                         ByVal strEstDistFromTo As String, ByVal strFare As String, ByVal strTotalFare As String, ByVal strTaxiNo As String,
                                         ByVal strDriverName As String, ByVal strTaxiProductType As String, ByVal strPromoCode As String,
                                         ByVal strPaymentMethod As String, ByVal strDiscount As String, ByVal strPickupTime As String, ByVal strToll As String,
                                         ByVal strCarModel As String) As Boolean

        Dim blnRet As Boolean = False
        Dim objMail As New aspNetEmail.EmailMessage
        Dim strEmail As String = strMemberEmail
        strEmail = Trim(strEmail)
        Dim strName As String = strMemberName
        Dim strEmailPath As String = ConfigurationManager.AppSettings("LocalSMTPPickup")
        Dim strEmailFrom As String = ""
        Dim strEmailFromName As String = ""
        Dim strEmailTo As String = ""
        Dim strEmailToName As String = ""
        Dim strEmailAccSubject As String = ""
        Dim strEmailCC As String = ""
        Dim strTemplatePath As String = strFilePath + ConfigurationManager.AppSettings("TemplatePath") + "\"
        Dim blnLocalSMTP As Boolean = System.Configuration.ConfigurationManager.AppSettings("UseLocalSMTP")
        Dim blnRegUseSMTP As Boolean = System.Configuration.ConfigurationManager.AppSettings("RegUseSMTP")
        Dim strAuthMethod As String = System.Configuration.ConfigurationManager.AppSettings("SMTPAuthMethod")
        Dim blnSMTPAuthReq As Boolean = System.Configuration.ConfigurationManager.AppSettings("SMTPAuthReq")

        Dim strSQL As String
        Dim objDS As DataSet
        Dim objDB As Database
        Dim strSMTP As String = ""
        Dim StrSMTPPW As String = ""
        Dim strSMTPServer As String = ""

        strSQL = "SELECT OrderID, TotalFare, TotalToll, PaymentType, CreateDate, TaxiDriver, TaxiNo,  "
        strSQL += "CASE WHEN IsBusiness = 1 THEN 'Business' ELSE 'Personal' END AS IsBusiness, "
        strSQL += "EstDistFromTo, AddFrom, AddTo "
        strSQL += "FROM tblOrder "
        strSQL += "WHERE RECID = '" & strAppSrc & "' "

        objDB = DatabaseFactory.CreateDatabase
        objDS = objDB.ExecuteDataSet(CommandType.Text, strSQL)

        If objDS.Tables(0).Rows.Count > 0 Then
            strEmailFromName = objDS.Tables(0).Rows(0)("FromName").ToString
            strEmailFrom = objDS.Tables(0).Rows(0)("FromEmail").ToString
            strEmailAccSubject = objDS.Tables(0).Rows(0)("Subject").ToString
            strTemplatePath += objDS.Tables(0).Rows(0)("Template").ToString
            strSMTP = objDS.Tables(0).Rows(0)("SMTPLogin").ToString
            StrSMTPPW = objDS.Tables(0).Rows(0)("SMTPPassword").ToString
            strSMTPServer = objDS.Tables(0).Rows(0)("SMTPServer").ToString

        End If

        Try
            objMail.ValidateAddress = False
            objMail.AddTo(strMemberEmail, strMemberName)
            objMail.FromAddress = strEmailFrom
            objMail.FromName = strEmailFromName
            objMail.Subject = strEmailAccSubject
            objMail.BodyFormat = aspNetEmail.MailFormat.Html

            objMail.GetBodyFromFile(strTemplatePath)
            objMail.Body = Replace(objMail.Body, "##LoginID##", strName, 1, , CompareMethod.Text)
            objMail.Body = Replace(objMail.Body, "##OrderID##", strOrderID, 1, , CompareMethod.Text)
            objMail.Body = Replace(objMail.Body, "##AddTo##", strAddTo, 1, , CompareMethod.Text)
            objMail.Body = Replace(objMail.Body, "##AddFrom##", strAddFrom, 1, , CompareMethod.Text)
            objMail.Body = Replace(objMail.Body, "##BookingDate##", strBookingDate, 1, , CompareMethod.Text)
            objMail.Body = Replace(objMail.Body, "##EstDistFromTo##", strEstDistFromTo, 1, , CompareMethod.Text)
            objMail.Body = Replace(objMail.Body, "##Fare##", strFare, 1, , CompareMethod.Text)
            objMail.Body = Replace(objMail.Body, "##TotalFare##", strTotalFare, 1, , CompareMethod.Text)
            objMail.Body = Replace(objMail.Body, "##TaxiNo##", strTaxiNo, 1, , CompareMethod.Text)
            objMail.Body = Replace(objMail.Body, "##DriverName##", strDriverName, 1, , CompareMethod.Text)
            objMail.Body = Replace(objMail.Body, "##TaxiProductType##", strTaxiProductType, 1, , CompareMethod.Text)
            objMail.Body = Replace(objMail.Body, "##PromoCode##", strPromoCode, 1, , CompareMethod.Text)
            objMail.Body = Replace(objMail.Body, "##PaymentMethod##", strPaymentMethod, 1, , CompareMethod.Text)
            objMail.Body = Replace(objMail.Body, "##Discount##", strDiscount, 1, , CompareMethod.Text)
            objMail.Body = Replace(objMail.Body, "##PickupTime##", strPickupTime, 1, , CompareMethod.Text)
            objMail.Body = Replace(objMail.Body, "##Toll##", strToll, 1, , CompareMethod.Text)
            objMail.Body = Replace(objMail.Body, "##CarModel##", strCarModel, 1, , CompareMethod.Text)


            If blnLocalSMTP = False Or blnRegUseSMTP = True Then
                objMail.Server = strSMTPServer
                'If strAppSrc.Equals("PublicCab") Then
                '    objMail.Server = "mail.publiccab.com"
                'Else
                '    objMail.Server = System.Configuration.ConfigurationManager.AppSettings("SMTPServer")
                'End If

                If blnSMTPAuthReq = True Then
                    objMail.Username = strSMTP
                    objMail.Password = StrSMTPPW

                    Select Case strAuthMethod.ToUpper()
                        Case "LOGIN"
                            objMail.SmtpAuthentication = aspNetEmail.SmtpAuthentication.AuthLogin
                        Case "TEXT"
                            objMail.SmtpAuthentication = aspNetEmail.SmtpAuthentication.AuthPlain
                        Case "MD5"
                            objMail.SmtpAuthentication = aspNetEmail.SmtpAuthentication.CramMD5
                    End Select
                End If
                objMail.IgnoreRecipientErrors = True
                objMail.Logging = True
                Try
                    objMail.Send()
                Catch ex As Exception
                    Dim strFileName As String = "Receipt_" + Format(Date.Now(), "yyyyMMdd") + ".log"
                    Dim strErr As String = ""
                    strErr = Format(Date.Now(), "yyyy-MM-dd hh:mm:ss tt") + vbTab
                    strErr += objMail.GetLog() + " " + strSQL + vbCrLf

                    strFileName = System.Web.HttpRuntime.AppDomainAppPath + "/Log/" + strFileName
                    My.Computer.FileSystem.WriteAllText(strFileName, strErr, True)

                    blnRet = False
                    Return blnRet
                End Try
            End If

            If blnLocalSMTP = True And blnRegUseSMTP = False Then
                objMail.SendToMSPickup(strEmailPath)
            End If

            blnRet = True

        Catch ex As Exception
            blnRet = False
            Dim strFileName As String = "Receipt_" + Format(Date.Now(), "yyyyMMdd") + ".log"
            Dim strErr As String = ""
            strErr = Format(Date.Now(), "yyyy-MM-dd hh:mm:ss tt") + vbTab
            strErr += ex.Message + " " + strSQL + vbCrLf

            strFileName = System.Web.HttpRuntime.AppDomainAppPath + "/Log/" + strFileName
            My.Computer.FileSystem.WriteAllText(strFileName, strErr, True)
        Finally
            objMail = Nothing
        End Try
        Return blnRet
    End Function
End Class
